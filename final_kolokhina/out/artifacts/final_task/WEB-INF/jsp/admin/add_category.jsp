<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Add category" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<c:choose>
	<c:when test="${not empty getAlert and getAlert=='success'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:when test="${not empty getAlert and getAlert=='addError'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#error-alert").show(1000);
				$("#error-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
</c:choose>
	<table id="main-container">

		<%@ include file="/WEB-INF/jspf/header.jspf" %>

		<div class="container">
		<tr>
			<td class="content">
				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="add_success"/>
				</div>

				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<fmt:message key="error_add"/>
				</div>
			<%-- CONTENT --%>
			<form id="request" action="controller" >
				<input type="hidden" name="command" value="addCategoryCommand"/>

				<h3><fmt:message key="admin.add.category.message"/></h3>
				<hr/>
				<input type="hidden" name="id" value=""/>
				<div class="form-group">
					<h4><fmt:message key="label.name"/></h4>
					<input type="text" name="name" id="name" class="form-control input-sm" value=""/>
				</div>

				<div class="form-group">
					<h4><fmt:message key="add.label.description"/></h4>
					<input type="text" name="description" id="description" class="form-control input-sm" value="" />
				</div>


				<%@ include file="/WEB-INF/jspf/add_dialog.jspf" %>

			</form>

			<%-- CONTENT --%>
			</td>
		</tr>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	</table>
	<script type="text/javascript">
		$('#confirm-add-button').on("click", function (){
			$("#request").submit();
		});
	</script>
</body>
