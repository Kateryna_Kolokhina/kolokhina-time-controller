<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Accounts" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<c:choose>
	<c:when test="${not empty getAlert and getAlert=='success'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:when test="${not empty getAlert and getAlert=='updateError'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#error-alert").show(1000);
				$("#error-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
</c:choose>

<table id="main-container">

	<%@ include file="/WEB-INF/jspf/header.jspf" %>

	<div class="container">
		<tr>
			<td class="content">

				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="update_success"/>
				</div>

				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<fmt:message key="error_update"/>
				</div>

				<%-- CONTENT --%>
				<form id="request" action="controller">
					<input type="hidden" name="command" value="updateAccount"/>
					<h3><fmt:message key="admin.accounts.message"/></h3>
					<hr/>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span>
						</div>
						<input type="text"  id="myInput" onkeyup="myFunction()"
							   class="form-control" placeholder="<fmt:message key="admin.accounts.search"/>" aria-label="Username" aria-describedby="basic-addon1">
					</div>
					<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
						<thead class="thead-light">

						<th>id</th>
						<th scope="col"> <fmt:message key="label.login"/></th>
						<th scope="col"><fmt:message key="label.email"/></th>
						<th scope="col"><fmt:message key="label.create"/></th>
						<th scope="col"><fmt:message key="label.access"/></th>
						<th></th>
						</thead>
						<tbody>
						<c:set var="k" value="0"/>

						<c:forEach var="item" items="${accountsList}">
							<c:set var="k" value="${k+1}"/>
							<tr>

								<td scope="row"><c:out value="${item.id}"/></td>

								<td>${item.login}</td>
								<td>${item.email}</td>
								<td>${item.createDate}</td>
<%--								<td>${item.access}</td>--%>
								<td>
									<c:choose>
										<c:when test="${item.access=='disable'}">
											<a role="button" class="btn btn-success"
													type ="submit" href="controller?command=updateAccount&id=${item.id}&access=enable&recordsPerPage=${recordsPerPage}&currentPage=${currentPage}">
												<fmt:message key="button.activate"/>
											</a>
										</c:when>
										<c:otherwise>
											<a role="button" type ="submit" class="btn btn-secondary"
													href="controller?command=updateAccount&id=${item.id}&access=disable&recordsPerPage=${recordsPerPage}&currentPage=${currentPage}">
												<fmt:message key="button.deactivate"/>
											</a>
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									<a role="button" class="btn btn-primary"
									   type ="submit" href="controller?command=viewAccount&id=${item.id}">
										<fmt:message key="button.view"/>
									</a>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>

					<nav aria-label="Navigation for admin">
						<ul class="pagination">
							<c:if test="${currentPage != 1}">
								<li class="page-item"><a class="page-link"
														 href="controller?command=listAccounts&recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}"><fmt:message key="pagination.previous"/></a>
								</li>
							</c:if>

							<c:forEach begin="1" end="${noOfPages}" var="i">
								<c:choose>
									<c:when test="${currentPage eq i}">
										<li class="page-item active"><a class="page-link">
												${i} <span class="sr-only">(current)</span></a>
										</li>
									</c:when>
									<c:otherwise>
										<li class="page-item"><a class="page-link"
																 href="controller?command=listAccounts&recordsPerPage=${recordsPerPage}&currentPage=${i}">${i}</a>
										</li>
									</c:otherwise>
								</c:choose>
							</c:forEach>

							<c:if test="${currentPage lt noOfPages}">
								<li class="page-item"><a class="page-link"
														 href="controller?command=listAccounts&recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}"><fmt:message key="pagination.next"/></a>
								</li>
							</c:if>
						</ul>
					</nav>
				</form>

<%--				 CONTENT --%>
			</td>
		</tr>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf" %>

	<script type="text/javascript">

		function myFunction() {
			// Declare variables
			var input, filter, table, tr, td, i, txtValue;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();
			table = document.getElementById("request_table");
			tr = table.getElementsByTagName("tr");

			// Loop through all table rows, and hide those who don't match the search query
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}

	</script>
</table>
</body>
