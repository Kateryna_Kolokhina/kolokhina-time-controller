<%@ page import="java.util.Calendar" %>
<%@ page import="ua.kolokhina.timeController.db.entity.Category" %>
<%@ page import="java.util.List" %>
<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Activities" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<c:choose>
	<c:when test="${not empty getAlert and getAlert=='success'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:when test="${not empty getAlert and getAlert=='updateError'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#error-alert").show(1000);
				$("#error-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
</c:choose>

<table id="main-container">

	<%@ include file="/WEB-INF/jspf/header.jspf" %>

	<div class="container">
		<tr>
			<td class="content">

				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="update_success"/>
				</div>

				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<fmt:message key="error_update"/>
				</div>

				<%-- CONTENT --%>
				<form id="request" action="controller">
					<input type="hidden" name="command" value="activitiesAdmin"/>
					<div class="container">
					<h3><fmt:message key="admin.activities.message"/> <a role="button" class="btn btn-primary"
										   type ="submit" href="controller?command=addActivity">
						<fmt:message key="button.add"/>
					</a> </h3>
					<hr/>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
								<h5><fmt:message key="filter.category.message"/></h5>
								<div class="form-group">
									<select id="category-filter" name = "category" class="form-select">
										<option value=""  >    </option>
										<c:forEach var="item" items="${listCategories}">
											<option value="${item.id}"  >${item.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<div class="col-xs-3 col-sm-3 col-md-3">
								<h5><fmt:message key="sort.message"/></h5>
								<div class="form-group">
									<select name = "sort" class="form-select">
										<option value="" selected>   </option>
										<option value="name" ${ (sort=='name') ? 'selected' : ''} ><fmt:message key="label.name"/></option>
										<option value="user_amount" ${ (sort=='user_amount') ? 'selected' : ''} ><fmt:message key="label.number_of_users"/></option>
										<option value="category_id" ${ (sort=='category_id') ? 'selected' : ''} ><fmt:message key="label.category"/></option>
									</select>
								</div>

							</div>
							<div class="col-xs-3 col-sm-3 col-md-3">
								<div class="form-group" style="margin-top: 32px;">
									<select name = "order" class="form-select">
										<option value="ascend"  ${ (order=='ascend') ? 'selected' : ''} ><fmt:message key="order.ascend"/></option>
										<option value="descend" ${ (order=='descend') ? 'selected' : ''} ><fmt:message key="order.descend"/></option>
									</select>
								</div>
							</div>
							<div class="col-xs-2 col-sm-2 col-md-2 d-flex align-items-end">
								<div class="form-group btn-block">
									<input type="submit" class="btn btn-primary btn-block" value='<fmt:message key="button.sort"/>'/>
								</div>
							</div>
						</div>
					</div>

					<table id="request_table" class="table table-bordred table-striped table-hover">
						<thead class="thead-light">

						<th>id</th>

						<th scope="col"><fmt:message key="label.category"/></th>
						<th scope="col"><fmt:message key="label.number_of_users"/></th>
						<th scope="col"><fmt:message key="label.name"/></th>
						<th scope="col"><fmt:message key="label.description"/></th>
						<th></th>
						<th></th>
						</thead>
						<tbody>
						<c:set var="k" value="0"/>

						<%
							List<Category> list = (List) request.getAttribute("listCategories");
						%>

						<c:forEach var="item" items="${activitiesList}">
							<c:set var="k" value="${k+1}"/>
							<tr>

								<td scope="row"><c:out value="${item.id}"/></td>

								<c:set var="index" value="${item.categoryId}"/>

								<%String test = pageContext.getAttribute("index").toString();
								Category category = list.get(Integer.parseInt(test)-1);
								%>
								<td><%=category.getName()%></td>
								<td>${item.userAmount}</td>
								<td>${item.name}</td>
								<td>${item.description}</td>

								<td><a role="button" class="btn btn-warning }"
									   href="controller?command=editActivity&id=${item.id}" >
									<fmt:message key="button.edit"/>
								</a>
								</td>
								<td>
									<c:choose>
										<c:when test="${item.access =='disable'}">
											<input class="btn btn btn-success actionUser"
												   type="button"
												   data-toggle="modal" data-target="#edit-file-modal"
												   id="delete_button"
												   name="${item.id}"
												   value ="<fmt:message key="button.activate"/>">
										</c:when>

										<c:otherwise>
											<input class="btn btn-secondary actionUser"
												   type="button"
												   data-toggle="modal" data-target="#edit-file-modal"
												   id="delete_button"
												   name="${item.id}"
												   value ="<fmt:message key="button.deactivate"/>">
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>

					<nav aria-label="Navigation for users">
						<ul class="pagination">
							<c:if test="${currentPage != 1}">
								<li class="page-item"><a class="page-link"
														 href="controller?command=activitiesAdmin&recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}&sort=${sort}&order=${order}&category=${category}">

									<fmt:message key="pagination.previous"/></a>
								</li>
							</c:if>

							<c:forEach begin="1" end="${noOfPages}" var="i">
								<c:choose>
									<c:when test="${currentPage eq i}">
										<li class="page-item active"><a class="page-link">
												${i} <span class="sr-only">(current)</span></a>
										</li>
									</c:when>
									<c:otherwise>
										<li class="page-item"><a class="page-link"
																 href="controller?command=activitiesAdmin&recordsPerPage=${recordsPerPage}&currentPage=${i}&sort=${sort}&order=${order}&category=${category}">${i}</a>

										</li>
									</c:otherwise>
								</c:choose>
							</c:forEach>

							<c:if test="${currentPage lt noOfPages}">
								<li class="page-item"><a class="page-link"
														 href="controller?command=activitiesAdmin&recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}&sort=${sort}&order=${order}&category=${category}">
									<fmt:message key="pagination.next"/></a>
								</li>
							</c:if>
						</ul>
					</nav>

					<input type="hidden" name ="recordsPerPage" value="${recordsPerPage}"/>
					<input type="hidden" name ="currentPage" value="${currentPage}"/>

					<%@ include file="/WEB-INF/jspf/edit_dialog.jspf" %>

				</form>
<%--				 CONTENT --%>
			</td>
		</tr>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf" %>


	<script type="text/javascript">

		var id;
		$(".actionUser").click(function(){
			id = $(this).prop("name");
		});
		$('#confirm-edit-button').on("click", function (){
			$('<input>', {
				type: 'hidden',
				name: 'activityId',
				value: id
			}).appendTo('#request');
			$('<input>', {
				type: 'hidden',
				name: 'update',
				value: 'update'
			}).appendTo('#request');
			$("#request").submit();
		});

		$('#category-filter').change(function() {
			// Declare variables
			var input, filter, table, tr, td, i, txtValue;

			filter = $(this).val();
			table = document.getElementById("request_table");
			tr = table.getElementsByTagName("tr");

			// Loop through all table rows, and hide those who don't match the search query
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
	});
	</script>
</table>
</body>
