<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Categories" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<c:choose>
	<c:when test="${not empty getAlert and getAlert=='success'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:when test="${not empty getAlert and getAlert=='deleteError'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#error-alert").show(1000);
				$("#error-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
</c:choose>


<table id="main-container">

	<%@ include file="/WEB-INF/jspf/header.jspf" %>

	<div class="container">
		<tr>
			<td class="content">


				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="delete_success"/>
				</div>

				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<fmt:message key="error_delete"/>
				</div>

				<%-- CONTENT --%>
				<form id="request" action="controller">
					<input type="hidden" name="command" value="deleteCategory"/>
					<h3><fmt:message key="admin.categories.message"/>  <a role="button" class="btn btn-primary"
										   type ="submit" href="controller?command=addCategory">
						<fmt:message key="button.add"/>
					</a> </h3>

					<hr/>
					<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
						<thead class="thead-light">

						<th>id</th>
						<th scope="col"><fmt:message key="label.name"/></th>
						<th scope="col"><fmt:message key="label.description"/></th>
						<th></th>
						<th></th>
						</thead>
						<tbody>
						<c:set var="k" value="0"/>

						<c:forEach var="item" items="${categoryList}">
							<c:set var="k" value="${k+1}"/>
							<c:if test="${item.id != 0}">
								<tr>

									<th scope="row"><c:out value="${item.id}"/></th>
									<td>${item.name}</td>
									<td>${item.description}</td>

									<td><a role="button" class="btn btn-success ${ (item.id==1) ? 'disabled' : ''}"
												href="controller?command=editCategory&id=${item.id}" >
										<fmt:message key="button.edit"/>
										</a>
									</td>
									<td>
										<input class="btn btn-danger actionUser"
												type="button"
												data-toggle="modal" data-target="#delete-file-modal"
												id="delete_button"
												name="${item.id}"
												value ="<fmt:message key="button.delete"/>" ${ (item.id==1) ? 'disabled="disabled"' : ''}>
									</td>
								</tr>
							</c:if>
						</c:forEach>

						</tbody>
					</table>


					<nav aria-label="Navigation for users">
						<ul class="pagination">
							<c:if test="${currentPage != 1}">
								<li class="page-item"><a class="page-link"
														 href="controller?command=listCategories&recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}&sort=${sort}"><fmt:message key="pagination.previous"/></a>
								</li>
							</c:if>

							<c:forEach begin="1" end="${noOfPages}" var="i">
								<c:choose>
									<c:when test="${currentPage eq i}">
										<li class="page-item active"><a class="page-link">
												${i} <span class="sr-only">(current)</span></a>
										</li>
									</c:when>
									<c:otherwise>
										<li class="page-item"><a class="page-link"
																 href="controller?command=listCategories&recordsPerPage=${recordsPerPage}&currentPage=${i}&sort=${sort}">${i}</a>

										</li>
									</c:otherwise>
								</c:choose>
							</c:forEach>

							<c:if test="${currentPage lt noOfPages}">
								<li class="page-item"><a class="page-link"
														 href="controller?command=listCategories&recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}&sort=${sort}"><fmt:message key="pagination.next"/></a>
								</li>
							</c:if>
						</ul>
					</nav>

					<%@ include file="/WEB-INF/jspf/delete_dialog.jspf" %>


					<input type="hidden" name ="recordsPerPage" value="${recordsPerPage}"/>
					<input type="hidden" name ="currentPage" value="${currentPage}"/>

				</form>
<%--				 CONTENT --%>
			</td>
		</tr>
	</div>
	<%@ include file="/WEB-INF/jspf/footer.jspf" %>

		<script type="text/javascript">

			var id;
			$(".actionUser").click(function(){
				id = $(this).prop("name");
			});

			$('#confirm-delete-button').on("click", function (){
				$('<input>', {
					type: 'hidden',
					name: 'id',
					value: id
				}).appendTo('#request');

				$("#request").submit();
			});
		</script>
</table>
</body>
