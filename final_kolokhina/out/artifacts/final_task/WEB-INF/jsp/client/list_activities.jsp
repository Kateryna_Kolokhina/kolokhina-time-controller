<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Add activities" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<c:choose>
	<c:when test="${not empty getAlert and getAlert=='add'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:when test="${not empty getAlert and getAlert=='addError'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#error-alert").show(1000);
				$("#error-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
</c:choose>

	<table id="main-container">

		<%@ include file="/WEB-INF/jspf/header.jspf" %>

		<div class="container">
		<tr>
			<td class="content">

				<c:if test="${not empty user and user.access=='disable'}">
					<div class="alert alert-warning" role="alert" id="access-alert" style="">
						<fmt:message key="user_access.not_allowed"/>
					</div>
				</c:if>

				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="request_status.success"/>
				</div>

				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<fmt:message key="error_add"/><br>
					<c:if test="${not empty errorMessage}">
						<c:out value="${errorMessage}"/>
					</c:if>

				</div>

			<form id="request" action="controller">
				<input type="hidden" name="command" value="addActivities"/>
				<h3><fmt:message key="list_activities_jsp.message"/></h3>
				<hr/>

				<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
					<thead class="thead-light">

							<th scope="col"></th>
							<th scope="col"><fmt:message key="label.name"/></th>
							<th scope="col"><fmt:message key="label.description"/></th>
					</thead>
					<tbody>
					<c:set var="k" value="0"/>


					<c:forEach var="category" items="${listCategories}">

					<tbody class="labels">
					<tr>
						<td colspan="3" scope="row">
							<label for="<c:out value="${category.id}"/>"><c:out value="${category.name}"/></label>
							<input type="checkbox" name="<c:out value="${category.id}"/>"
								   id="<c:out value="${category.id}"/>" data-toggle="toggle">
						</td>
					</tr>
					</tbody>

					<tbody class="hide" style="display: none;">
						<c:forEach var="item" items="${listActivitiesItems}">
							<c:if test="${item.categoryId == category.id}">
								<tr>
									<td><input type="checkbox" name="itemId" value="${item.id}"/></td>
									<td>${item.name}</td>
									<td>${item.description}</td>
								</tr>
							</c:if>
						</c:forEach>
					</tbody>
					</c:forEach>

					</tbody>
				</table>

				<input id="actionUser" type="submit" class="btn btn-secondary actionUser" name="add"
					   value='<fmt:message key="list_activities_jsp.button.make_an_request"/>'
				${ (not empty user and user.access=='disable') ? 'disabled="disabled"' : ''}
				/>

			</form>

			<%-- CONTENT --%>
			</td>
		</tr>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf" %>

	</table>
</body>
