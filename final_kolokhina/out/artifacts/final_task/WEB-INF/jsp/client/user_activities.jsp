<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Change activities" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<c:choose>
	<c:when test="${not empty getAlert and getAlert=='success'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${getAlert=='editError' }">
				<script>
					$(document).ready(function() {
						$("#error-alert").show(1000);
						$("#error-edit").show(1000);
						$("#error-edit").show().delay(7000).fadeOut();
						$("#error-alert").show().delay(7000).fadeOut();
					});
				</script>
			</c:when>
			<c:when test="${getAlert=='deleteError' }">
				<script>
					$(document).ready(function() {
						$("#error-alert").show(1000);
						$("#error-delete").show(1000);

						$("#error-delete").show().delay(7000).fadeOut();
						$("#error-alert").show().delay(7000).fadeOut();
					});
				</script>
			</c:when>
		</c:choose>
		<c:set var="getAlert" value="" scope="session" />
	</c:otherwise>
</c:choose>

<table id="main-container">

		<%@ include file="/WEB-INF/jspf/header.jspf" %>

		<div class="container">
		<tr>
			<td class="content">

				<c:if test="${not empty user and user.access=='disable'}">
					<div class="alert alert-warning" role="alert" id="access-alert" style="">
						<fmt:message key="user_access.not_allowed"/>
					</div>
				</c:if>

				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="request_status.success"/>
				</div>
				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<p  id="error-edit" style="display:none;"><fmt:message key="error_edit"/></p>
					<p id="error-delete" style="display:none;"><fmt:message key="error_delete"/></p>
				</div>

			<%-- CONTENT --%>
			<form id="request" action="controller" class="myForm">
				<input type="hidden" name="command" value="editActivities"/>

				<h3><fmt:message key="user_activities_edit.message"/></h3>

				<hr/>
				<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
					<thead class="thead-light">

							<th>id</th>
							<th scope="col"><fmt:message key="button.delete"/></th>
							<th scope="col"><fmt:message key="label.name"/></th>
							<th scope="col"><fmt:message key="table.header.before"/></th>
							<th scope="col"><fmt:message key="table.header.new"/></th>
<%--							<th scope="col"><fmt:message key="table.header.status"/></th>--%>
					</thead>
					<tbody>
					<c:set var="k" value="0"/>

					<c:forEach var="category" items="${listCategories}">

					<tbody class="labels">
					<tr>
						<td colspan="5" scope="row">
							<label for="<c:out value="${category.id}"/>"><c:out value="${category.name}"/></label>
							<input type="checkbox" name="<c:out value="${category.id}"/>"
								   id="<c:out value="${category.id}"/>" data-toggle="toggle">
						</td>
					</tr>
					</tbody>

					<tbody class="hide">
					<c:forEach var="item" items="${userActivities}">
						<c:if test="${item.categoryId == category.id}">
							<tr>
										<th scope="row"><c:out value="${item.id}"/></th>
										<td><input type="checkbox" name="deleteId" value="${item.id}"/></td>
										<td>${item.name}</td>
										<td>${item.timeAmount}</td>
										<td> <input type="text" class="timepicker" name="timeAmount" value="${item.timeAmount}"/></td>
											<%--							<td>${item.statusId}</td>--%>
							</tr>
						</c:if>
					</c:forEach>
					</tbody>
					</c:forEach>
					</tbody>
				</table>


				<button class="btn btn-secondary actionUser"
						type="button"
						data-toggle="modal" data-target="#edit-file-modal"
						${ (not empty user and user.access=='disable') ? 'disabled="disabled"' : ''}>
					<fmt:message key="button.edit"/>
				</button>

				<button class="btn btn-danger actionUser"
						type="button"
						data-toggle="modal" data-target="#delete-file-modal"
				${ (not empty user and user.access=='disable') ? 'disabled="disabled"' : ''}>
					<fmt:message key="button.delete"/>
				</button>

				<%@ include file="/WEB-INF/jspf/edit_dialog.jspf" %>
				<%@ include file="/WEB-INF/jspf/delete_dialog.jspf" %>


			</form class="myForm">

				<div class="myForm">
					<h3><fmt:message key="user_activities_requests.message"/></h3>
					<hr/>

					<c:forEach var="item" items="${statuses}">
						<p>${item.name} - ${item.description}</p>
					</c:forEach>
					<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
						<thead class="thead-light">

						<th>id</th>
						<th scope="col"><fmt:message key="label.name"/></th>
						<th scope="col"><fmt:message key="table.header.status"/></th>
						</thead>
						<tbody>
						<c:forEach var="item" items="${requests}">
								<tr>
									<th scope="row"><c:out value="${item.activity_id}"/></th>
									<td>${item.name}</td>
									<td>${item.statusName}</td>
								</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			<%-- CONTENT --%>
			</td>
		</tr>
			<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</table>
		</div>
<script type="text/javascript">


	$('#confirm-delete-button').on("click", function (){
		$('#request').append('<input type="hidden" name="delete" value="delete" />');
		$("#request").submit();
	});
	$('#confirm-edit-button').on("click", function (){
		$('#request').append('<input type="hidden" name="edit" value="edit" />');
		$("#request").submit();
	});
</script>


</body>
