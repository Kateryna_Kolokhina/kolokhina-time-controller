<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Requests" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<c:choose>
	<c:when test="${not empty getAlert and getAlert=='success'}">
		<c:set var="getAlert" value="" scope="session" />
		<script>
			$(document).ready(function() {
				$("#success-alert").show(1000);
				$("#success-alert").show().delay(5000).fadeOut();
			});
		</script>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${getAlert=='confirmError' }">
				<script>
					$(document).ready(function() {
						$("#error-alert").show(1000);
						$("#error-confirm").show(1000);
						$("#error-confirm").show().delay(7000).fadeOut();
						$("#error-alert").show().delay(7000).fadeOut();
					});
				</script>
			</c:when>
			<c:when test="${getAlert=='cancelError' }">
				<script>
					$(document).ready(function() {
						$("#error-alert").show(1000);
						$("#error-cancel").show(1000);

						$("#error-cancel").show().delay(7000).fadeOut();
						$("#error-alert").show().delay(7000).fadeOut();
					});
				</script>
			</c:when>
		</c:choose>
		<c:set var="getAlert" value="" scope="session" />
	</c:otherwise>
</c:choose>

<table id="main-container">

		<%@ include file="/WEB-INF/jspf/header.jspf" %>

		<div class="container">
		<tr>
			<td class="content">

				<div class="alert alert-success" role="alert" id="success-alert" style="display:none;">
					<fmt:message key="request_status.success"/>
				</div>
				<div class="alert alert-danger" role="alert" id="error-alert" style="display:none;">
					<p  id="error-confirm" style="display:none;"><fmt:message key="error_confirm"/></p>
					<p id="error-cancel" style="display:none;"><fmt:message key="error_cancel"/></p>
				</div>

			<%-- CONTENT --%>
				<form id="add_request" action="controller" class="myForm">
					<input type="hidden" name="command" value="editRequestsCommand"/>
					<h3><fmt:message key="request.add.message"/></h3>
					<hr/>
					<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
						<thead class="thead-light">

								<th><fmt:message key="label.login"/></th>
								<th scope="col"><fmt:message key="label.name"/></th>
								<th><fmt:message key="button.confirm"/></th>
								<th><fmt:message key="button.cancel"/></th>
						</thead>
						<tbody>
						<c:set var="k" value="0"/>

						<c:forEach var="item" items="${requestListAdd}">
								<tr class="group">
									<td>${item.login}</td>
									<td>${item.name}</td>
									<td><input type="checkbox"  class="radio" name="${k}" value="confirm"/></td>
									<td><input type="checkbox" class="radio"  name="${k}" value="cancel"/></td>
								</tr>
							<c:set var="k" value="${k+1}"/>
						</c:forEach>
						</tbody>
					</table>
					<button class="btn btn-secondary actionUser"
							type="button"
							data-toggle="modal" data-target="#edit-file-modal">
						<fmt:message key="button.edit"/>
					</button>

					<%@ include file="/WEB-INF/jspf/edit_dialog.jspf" %>
			</form>

				<form id="delete_request" action="controller" class="myForm">
					<input type="hidden" name="command" value="editRequestsCommand"/>
					<h3><fmt:message key="request.delete.message"/></h3>
					<hr/>
					<table id="request_table" class="table table-bordred table-striped table-sm table-hover">
						<thead class="thead-light">

						<th><fmt:message key="label.login"/></th>
						<th scope="col"><fmt:message key="label.name"/></th>
						<th><fmt:message key="button.confirm"/></th>
						<th><fmt:message key="button.cancel"/></th>
						</thead>
						<tbody>
						<c:set var="k" value="0"/>

						<c:forEach var="item" items="${requestListDelete}">
							<tr class="group">
								<td>${item.login}</td>
								<td>${item.name}</td>
								<td><input type="checkbox"  class="radio" name="${k}" value="confirm"/></td>
								<td><input type="checkbox" class="radio"  name="${k}" value="cancel"/></td>
							</tr>
							<c:set var="k" value="${k+1}"/>
						</c:forEach>
						</tbody>
					</table>

					<button class="btn btn-danger actionUser"
							type="button"
							data-toggle="modal" data-target="#delete-file-modal">
						<fmt:message key="button.edit"/>
					</button>

					<div class="modal fade" id="edit-file-modal" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel"><fmt:message key="confirm.message.request.add"/></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form class="form-horizontal" method="post" id="edit_form">
									<div class="modal-footer">
										<button  data-dismiss="modal" class="btn btn-primary"  id="confirm-edit-button">
											<fmt:message key="button.edit"/>
										</button>
										<button data-dismiss="modal" class="btn btn-default" name="in_confirm_insert" id="cancel-delete-button"><fmt:message key="button.cancel"/></button>
									</div>
								</form>
							</div>
						</div>
					</div>


					<div class="modal fade" id="delete-file-modal" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel"><fmt:message key="confirm.message.request.delete"/></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form class="form-horizontal" method="post" id="delete_form">
									<div class="modal-footer">
										<button  data-dismiss="modal" class="btn btn-danger"  id="confirm-delete-button">
											<fmt:message key="button.delete"/>
										</button>
										<button data-dismiss="modal" class="btn btn-default" name="in_confirm_insert" id="cancel-delete-button"><fmt:message key="button.cancel"/></button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</form>
			<%-- CONTENT --%>
			</td>
		</tr>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</table>
<script type="text/javascript">


	$("input:checkbox").on('click', function() {
		// in the handler, 'this' refers to the box clicked on
		var $box = $(this);
		if ($box.is(":checked")) {
			// the name of the box is retrieved using the .attr() method
			// as it is assumed and expected to be immutable
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			// the checked state of the group/box on the other hand will change
			// and the current value is retrieved using .prop() method
			$(group).prop("checked", false);
			$box.prop("checked", true);
		} else {
			$box.prop("checked", false);
		}
	});

	$('#confirm-edit-button').on("click", function (){

		$('#add_request').append('<input type="hidden" name="add" value="add" />');
		$("#add_request").submit();
	});

	$('#confirm-delete-button').on("click", function (){
		$('#delete_request').append('<input type="hidden" name="delete" value="delete" />');
		$("#delete_request").submit();
	});

</script>
</body>
