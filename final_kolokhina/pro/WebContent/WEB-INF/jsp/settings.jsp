<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Settings" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<table id="main-container">

	<%@ include file="/WEB-INF/jspf/header.jspf" %>

	<div class="container">
		<tr>
			<td class="content">
				<%-- CONTENT --%>


				<form id="settings_form" action="controller" method="get">
					<input type="hidden" name="command" value="updateSettings" />

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<label class="input-group-text" for="chooseLocale"><fmt:message key="settings_jsp.label.localization"/></label>
						</div>
						<select name="localeToSet" id="chooseLocale" class="custom-select">
							<c:choose>
								<c:when test="${not empty defaultLocale}">
									<option value="">${defaultLocale}[Default]</option>
								</c:when>
								<c:otherwise>
									<option value=""/>
								</c:otherwise>
							</c:choose>

							<c:forEach var="localeName" items="${locales}">
								<option value="${localeName}">${localeName}</option>
							</c:forEach>
						</select>
					</div>

					<%--					<div>--%>
					<%--						<p>--%>
					<%--							<fmt:message key="settings_jsp.label.first_name"/>--%>
					<%--						</p>--%>
					<%--						<input name="firstName">--%>
					<%--					</div>--%>

					<input type="submit" class="btn btn-secondary" value='<fmt:message key="settings_jsp.button.update"/>'><br/>
				</form>

				<%-- CONTENT --%>
			</td>
		</tr>

		<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	</div>
</table>
</body>
</html>