<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Login" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
<div class="container">
    <div class="row centered-form myFormHello">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><fmt:message key="login_jsp.message.welcome"/></h3>

                    <p><a href="registration.jsp">
                        <fmt:message key="header_jspf.anchor.registration"/></a>
                    </p>

                </div>
                <div class="panel-body">
                    <form role="form"  id="login_form" name="login_form"
                          action="controller" method="post"  >

                        <input type="hidden" name="command" value="login"/>

                        <div class="form-group">
                            <input type="text" name="login" id="login" class="form-control input-sm" placeholder="<fmt:message key="login_jsp.label.login"/>">
                        </div>

                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control input-sm" placeholder="<fmt:message key="login_jsp.label.password"/>">
                        </div>

                        <input type="submit" value="<fmt:message key="login_jsp.button.login"/>" class="btn btn-info btn-block">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%--&lt;%&ndash;=========================================================================== --%>
<%--Here we use a table layout.--%>
<%--Class page corresponds to the '.page' element in included CSS document.--%>
<%--===========================================================================&ndash;%&gt; --%>
<%--	<table id="main-container">--%>

<%--&lt;%&ndash;=========================================================================== --%>
<%--This is the HEADER, containing a top menu.--%>
<%--header.jspf contains all necessary functionality for it.--%>
<%--Just included it in this JSP document.--%>
<%--===========================================================================&ndash;%&gt; --%>

<%--		&lt;%&ndash; HEADER &ndash;%&gt;--%>
<%--		&lt;%&ndash; <%@ include file="/WEB-INF/jspf/header.jspf"%> &ndash;%&gt;--%>
<%--		&lt;%&ndash; HEADER &ndash;%&gt;--%>

<%--&lt;%&ndash;=========================================================================== --%>
<%--This is the CONTENT, containing the main part of the page.--%>
<%--===========================================================================&ndash;%&gt; --%>
<%--		<tr >--%>
<%--			<td class="content center">--%>
<%--			&lt;%&ndash; CONTENT &ndash;%&gt;--%>
<%--			--%>
<%--&lt;%&ndash;=========================================================================== --%>
<%--Defines the web form.--%>
<%--===========================================================================&ndash;%&gt; --%>
<%--				<form id="login_form" action="controller" method="post">--%>

<%--&lt;%&ndash;=========================================================================== --%>
<%--Hidden field. In the query it will act as command=login.--%>
<%--The purpose of this to define the command name, which have to be executed --%>
<%--after you submit current form. --%>
<%--===========================================================================&ndash;%&gt; --%>
<%--					<input type="hidden" name="command" value="login"/>--%>

<%--					<fieldset >--%>
<%--						<legend>--%>
<%--							<fmt:message key="login_jsp.label.login"/>--%>
<%--						</legend>--%>
<%--						<input name="login"/><br/>--%>
<%--					</fieldset><br/>--%>
<%--					<fieldset>--%>
<%--						<legend>--%>
<%--							<fmt:message key="login_jsp.label.password"/>--%>
<%--						</legend>--%>
<%--						<input type="password" name="password"/>--%>
<%--					</fieldset><br/>--%>
<%--					--%>
<%--					<input type="submit" value='<fmt:message key="login_jsp.button.login"/>'>								--%>
<%--				</form>--%>
<%--				--%>
<%--			&lt;%&ndash; CONTENT &ndash;%&gt;--%>

<%--			</td>--%>
<%--		</tr>--%>

<%--		<%@ include file="/WEB-INF/jspf/footer.jspf"%>--%>
<%--		--%>
<%--	</table>--%>
</body>
</html>