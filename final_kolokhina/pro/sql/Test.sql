-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema final_project
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `test` ;

-- -----------------------------------------------------
-- Schema final_project
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 ;
USE `test` ;

-- -----------------------------------------------------
-- Table `final_project`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`role` ;

CREATE TABLE IF NOT EXISTS `test`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(1024) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


INSERT INTO `role` (`id`,`name`,`description`) VALUES (1,'admin','test');
INSERT INTO `role` (`id`,`name`,`description`) VALUES (2,'client','test');

-- -----------------------------------------------------
-- Table `final_project`.`account`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`account` ;

CREATE TABLE IF NOT EXISTS `test`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(50) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` INT NOT NULL,
  `access` ENUM('enable', 'disable') NOT NULL DEFAULT 'disable',
  PRIMARY KEY (`id`),
  INDEX `fk_user_role_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_role`
    FOREIGN KEY (`role_id`)
    REFERENCES `test`.`role` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8;


INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (2,'ivanov','ivanov@gmail.com','kokokewe','2021-02-03 02:39:39',1,'enable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (3,'fedor','fedor@gmail.com','papapapap','2021-02-03 02:39:39',1,'enable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (7,'kokok','koko@gmail.com','nRMVe7hFPe','2021-02-03 15:37:35',1,'disable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (17,'cLogin','test@email.com','password','2021-02-05 16:57:44',1,'disable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (19,'Test01','kok@gmail.com','Nmsftgsf00','2021-02-05 17:10:08',1,'enable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (20,'User12','kolokhina@gmail.com','Nmsftgsf00','2021-02-08 22:22:30',1,'enable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (21,'User13','kolokhina@gmail.com','Nmsftgsf00','2021-02-08 22:51:11',1,'disable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (22,'User14','kolokhina@gmail.com','Nmsftgsf00','2021-02-09 01:10:14',1,'enable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (23,'AdminKate','kolokh@gmail.com','$2a$12$dwjqeI31HNQ6RTx6IzvXcO9V4INpAqssyUy3RAj/mAAvVcwiPoOgS','2021-02-22 16:47:52',2,'enable');
INSERT INTO `account` (`id`,`login`,`email`,`password`,`create_time`,`role_id`,`access`) VALUES (24,'UserKate','kok@gmail.com','$2a$12$oZMyDmPJ.SkZE2kntbchFevF28q72xl0H.gouCvLE1WEocORFSlN6','2021-02-22 16:49:23',1,'enable');


-- -----------------------------------------------------
-- Table `final_project`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`category` ;
CREATE TABLE IF NOT EXISTS `test`.`category` (
  `category_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(1024) NOT NULL DEFAULT 'none description',
  PRIMARY KEY (`category_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


INSERT INTO `category` (`category_id`,`name`,`description`) VALUES (1,'none','none description');
INSERT INTO `category` (`category_id`,`name`,`description`) VALUES (2,'needs','test');
INSERT INTO `category` (`category_id`,`name`,`description`) VALUES (3,'wrt','testworss');
INSERT INTO `category` (`category_id`,`name`,`description`) VALUES (4,'hobby','test description');
INSERT INTO `category` (`category_id`,`name`,`description`) VALUES (5,'lol','none description');


-- -----------------------------------------------------
-- Table `final_project`.`activity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`activity` ;
CREATE TABLE IF NOT EXISTS `test`.`activity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(1024) NOT NULL DEFAULT 'none description',
  `user_amount` INT(10) UNSIGNED ZEROFILL NOT NULL DEFAULT '0000000000',
  `category_id` INT NULL DEFAULT '1',
  `access` ENUM('enable', 'disable') NOT NULL DEFAULT 'enable',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  INDEX `fk_activity_category1_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_activity_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `test`.`category` (`category_id`)
    ON DELETE SET NULL)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8;


INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (1,'sleeping','a type or rest',0000000010,2);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (2,'coding','write a code',0000000033,3);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (3,'drawing','draw a picture',0000000002,4);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (4,'cycling','ride a bicycle',0000000003,4);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (5,'printing','print the documents',0000000001,3);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (6,'reading','read a book',0000000004,2);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (7,'studying','study at school or etc.',0000000100,2);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (8,'take pictures','make a photo of smth or smb',0000000050,4);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (9,'dancing','just dancing',0000000003,4);
INSERT INTO `activity` (`id`,`name`,`description`,`user_amount`,`category_id`) VALUES (10,'testDelete','test',0000000001,1);


-- -----------------------------------------------------
-- Table `final_project`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`status` ;
CREATE TABLE IF NOT EXISTS `test`.`status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(1024) NOT NULL DEFAULT 'none description',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;

INSERT INTO `status` (`id`,`name`,`description`) VALUES (1,'add','the administrator is checking your adding request');
INSERT INTO `status` (`id`,`name`,`description`) VALUES (2,'confirmed','the administrator has confirmed your request');
INSERT INTO `status` (`id`,`name`,`description`) VALUES (3,'canceled','the administrator denied your request');
INSERT INTO `status` (`id`,`name`,`description`) VALUES (4,'delete','the administrator is checking your deleting request');

-- -----------------------------------------------------
-- Table `final_project`.`accounts_activities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `test`.`accounts_activities` ;
CREATE TABLE IF NOT EXISTS `test`.`accounts_activities` (
  `account_id` INT NOT NULL,
  `activity_id` INT NULL DEFAULT '0',
  `time_amount` TIME NOT NULL DEFAULT '00:00:00',
  `status_id` INT NOT NULL,
  UNIQUE INDEX `account_id` (`account_id` ASC, `activity_id` ASC) VISIBLE,
  INDEX `fk_account_has_activity_activity1_idx` (`activity_id` ASC) VISIBLE,
  INDEX `fk_accounts_activities_status1_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_account_has_activity_account1_idx` (`account_id` ASC) VISIBLE,
  CONSTRAINT `fk_account_has_activity_account1`
    FOREIGN KEY (`account_id`)
    REFERENCES `test`.`account` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `fk_account_has_activity_activity1`
    FOREIGN KEY (`activity_id`)
    REFERENCES `test`.`activity` (`id`)
    ON DELETE RESTRICT,
  CONSTRAINT `fk_accounts_activities_status1`
    FOREIGN KEY (`status_id`)
    REFERENCES `test`.`status` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `test`;

DELIMITER $$
USE `test`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `test`.`category_AFTER_DELETE`
AFTER DELETE ON `test`.`category`
FOR EACH ROW
BEGIN
  update activity set category_id = 1 where category_id = 1 is null;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `accounts_activities` (`account_id`,`activity_id`,`time_amount`,`status_id`) VALUES (24,2,'00:01:00',2);
INSERT INTO `accounts_activities` (`account_id`,`activity_id`,`time_amount`,`status_id`) VALUES (24,7,'00:03:00',2);
INSERT INTO `accounts_activities` (`account_id`,`activity_id`,`time_amount`,`status_id`) VALUES (24,10,'00:03:00',2);


