package ua.kolokhina.timeController;


public final class Fields {
	
	// entities
	public static final String ENTITY__ID = "id";
	public static final String CATEGORY__ID = "category_id";
	
	public static final String USER__LOGIN = "login";
	public static final String USER__PASSWORD = "password";
	public static final String USER__EMAIL = "email";
	public static final String USER__CREATE_TIME = "create_time";
	public static final String USER__ROLE_ID = "role_id";

	public static final String ACCESS = "access";

	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";

	public static final String ACTIVITY__USER_AMOUNT = "user_amount";
	public static final String ACTIVITY__CATEGORY_ID = "category_id";

	public static final String ACTIVITY__ACCOUNT_ID = "account_id";
	public static final String ACTIVITY__ACTIVITY_ID = "activity_id";

	public static final String ACTIVITY__STATUS_ID = "status_id";
	public static final String ACTIVITY__STATUS_NAME = "stat_name";
	public static final String ACTIVITY__TIME_AMOUNT = "time_amount";

	
	
}