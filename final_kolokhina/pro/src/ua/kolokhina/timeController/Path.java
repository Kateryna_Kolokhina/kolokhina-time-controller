package ua.kolokhina.timeController;


public final class Path {

	public static final String PAGE__LOGIN = "/login.jsp";
	public static final String PAGE__ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";

	public static final String PAGE__SETTINGS = "/WEB-INF/jsp/settings.jsp";
	public static final String COMMAND__VIEW_SETTINGS = "/controller?command=viewSettings";

	public static final String COMMAND__USER_ACTIVITIES = "/controller?command=userActivities";
	public static final String PAGE__USER_ACTIVITIES = "/WEB-INF/jsp/client/user_activities.jsp";


	public static final String PAGE__LIST_ACTIVITIES = "/WEB-INF/jsp/client/list_activities.jsp";
	public static final String COMMAND__LIST_ACTIVITIES = "/controller?command=listActivities";

	public static final String PAGE__ADMIN_ADD_ACTIVITY = "/WEB-INF/jsp/admin/add_activity.jsp";

	public static final String PAGE__ADMIN_ADD_CATEGORY = "/WEB-INF/jsp/admin/add_category.jsp";

	public static final String PAGE__ADMIN_LIST_ACCOUNTS = "/WEB-INF/jsp/admin/list_accounts.jsp";
	public static final String COMMAND__ADMIN_LIST_ACCOUNTS = "/controller?command=listAccounts";


	public static final String PAGE__ADMIN_LIST_REQUESTS = "/WEB-INF/jsp/admin/requests.jsp";
	public static final String COMMAND__ADMIN_LIST_REQUESTS = "/controller?command=requests";


	public static final String PAGE__ADMIN_USER_INFO = "/WEB-INF/jsp/admin/user_info.jsp";

	public static final String PAGE__ADMIN_LIST_ACTIVITIES = "/WEB-INF/jsp/admin/list_activities.jsp";

	public static final String PAGE__ADMIN_LIST_CATEGORIES = "/WEB-INF/jsp/admin/list_categories.jsp";
	public static final String COMMAND__ADMIN_LIST_CATEGORIES = "/controller?command=listCategories";

	public static final String PAGE__ADMIN_EDIT_CATEGORY = "/WEB-INF/jsp/admin/edit_category.jsp";
	public static final String COMMAND__ADMIN_EDIT_CATEGORY = "/controller?command=editCategory";

	public static final String PAGE__ADMIN_EDIT_ACTIVITY = "/WEB-INF/jsp/admin/edit_activity.jsp";
	public static final String COMMAND__ADMIN_EDIT_ACTIVITY = "/controller?command=editActivity";

}