package ua.kolokhina.timeController;

import ua.kolokhina.timeController.db.entity.User;


/**
 *  Enum of user roles.
 *
 * @author K.Kolokhina
 */
public enum Role {
	NULL,CLIENT,ADMIN;
	
	public static Role getRole(User user) {
		int roleId = user.getRole();

		return Role.values()[roleId];
	}
	
	public String getName() {
		return name().toLowerCase();
	}
	
}
