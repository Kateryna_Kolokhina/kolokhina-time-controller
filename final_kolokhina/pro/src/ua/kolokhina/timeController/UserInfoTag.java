package ua.kolokhina.timeController;

import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * Custom Tag to display information about user.
 *
 * @author K.Kolokhina
 */
public class UserInfoTag extends SimpleTagSupport {
    private Role role;
    private User user;

    public UserInfoTag() {
    }

    @Override
    public void doTag() throws JspException {
        try {
            String formattedNumber;
            if(role.getName().equals("client"))
                formattedNumber = "("+user.getLogin()+") - "+ role.getName()+" | "+user.getAccess();
            else
                formattedNumber = "("+user.getLogin()+") - "+ role.getName();
            getJspContext().getOut().write(formattedNumber);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SkipPageException("Exception in formatting");
        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
