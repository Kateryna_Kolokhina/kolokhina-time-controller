package ua.kolokhina.timeController.db;

import ua.kolokhina.timeController.Fields;
import ua.kolokhina.timeController.db.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Activities entity.
 */
public class ActivitiesDao {

    private DBManager dbManager;

    private static final String SQL__FIND_ALL_STATUS_ITEMS =
            "SELECT * FROM status order by id";

    private static final String SQL__FIND_ALL_ACTIVITIES_ITEMS =
            "SELECT * FROM activity order by id";

    private static final String SQL_FIND_ACTIVITIES_ENABLED =
            "SELECT * from activity where access = 'enable' ";

    private static final String SQL_FIND_ACTIVITY_BY_ID =
            "SELECT * from activity where id = ?";

    private static final String SQL_FIND_ACTIVITY_BY_NAME =
            "SELECT * from activity where name = 'requestName'";

    private static final String SQL__FIND_ACCOUNT_ACTIVITIES =
            "SELECT act.id,act.name,act.category_id,ac.time_amount, ac.status_id" +
                    " FROM accounts_activities ac " +
                    "INNER JOIN activity act ON act.id = ac.activity_id" +
                    " where account_id = ? and status_id = ? order by id";

    private static final String SQL__FIND_ACCOUNTS_REQUESTS =
            "SELECT ac_act.account_id,acc.login,ac_act.activity_id,act.name, ac_act.status_id,stat.name as stat_name\n" +
                    "FROM accounts_activities ac_act \n" +
                    "INNER JOIN activity act ON ac_act.activity_id = act.id\n" +
                    "INNER JOIN account acc ON ac_act.account_id = acc.id\n" +
                    "where status_id = ?";

    private static final String SQL__FIND_USER_REQUESTS =
            "SELECT ac_act.account_id,acc.login, ac_act.activity_id, act.name, ac_act.status_id, stat.name as stat_name\n" +
                    "FROM accounts_activities ac_act \n" +
                    "INNER JOIN activity act ON ac_act.activity_id = act.id\n" +
                    "INNER JOIN account acc ON ac_act.account_id = acc.id\n" +
                    "INNER JOIN status stat ON ac_act.status_id =stat.id\n" +
                    "where ac_act.account_id = ? and status_id NOT LIKE 2  order by status_id";

    private static final String SET_ACTIVITY_TO_USER = "INSERT IGNORE INTO " +
            "accounts_activities (account_id, activity_id, status_id) VALUES (?, ?, ?)";

    private static final String UPDATE_ACTIVITY_ACCESS_TO_USER = "UPDATE accounts_activities SET status_id = ? WHERE account_id = ? and activity_id = ?";

    private static final String UPDATE_ACTIVITY_TO_USER =
            "UPDATE accounts_activities SET time_amount = ? WHERE account_id = ? and activity_id = ? ";

    private static final String FIND_REQUEST_FROM_USER =
            "SELECT status_id FROM accounts_activities WHERE account_id = ? and activity_id = ?";

    private static final String CANCEL_REQUEST_FROM_USER =
            "UPDATE accounts_activities SET status_id = 3 WHERE account_id = ? and activity_id = ?";

    private static final String DELETE_REQUEST_FROM_USER =
            "DELETE FROM accounts_activities WHERE account_id = ? and activity_id = ?";

    private static final String DELETE_ACTIVITY_FROM_USER =
    "UPDATE accounts_activities SET status_id = 4 WHERE account_id = ? and activity_id = ?";

    private static final String SQL__FIND_ALL_ACTIVITY_SORTED = "SELECT * FROM activity order by sort orderType LIMIT ?, ?";

    private static final String SQL__DELETE_REQUEST_FROM_USER = "UPDATE activity SET user_amount = user_amount-1 WHERE id = ?";

    private static final String SQL__CONFIRM_REQUEST_FROM_USER = "UPDATE activity SET user_amount = user_amount +1 WHERE id = ?";
    private static final String SQL__ROWS_ACTIVITY = "SELECT COUNT(id) from activity";

    private static final String SQL__UPDATE_ACCESS_ACTIVITY = "UPDATE activity SET access='status' WHERE id=?";
    private static final String SQL__ADD_ACTIVITY = "INSERT INTO activity (name,description,category_id) VALUES (?,?,?)";

    public ActivitiesDao(){
        dbManager = DBManager.getInstance();
    }
    public ActivitiesDao(boolean test){
        dbManager = DBManager.getInstance(test);
    }

    /**
     * Find all activities with some access status.
     *
     * @param access enable or disable
     * @return list of activities
     */
    public List<Activity> findActivities(String access) {
        List<Activity> activitiesList = new ArrayList<Activity>();
        Statement stmt;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            ActivityItemMapper mapper = new ActivityItemMapper();
            stmt = con.createStatement();
            if(access.equals("enable"))
                rs = stmt.executeQuery(SQL_FIND_ACTIVITIES_ENABLED);
            else if(access == null)
                rs = stmt.executeQuery(SQL__FIND_ALL_ACTIVITIES_ITEMS);
            while (rs.next())
                activitiesList.add(mapper.mapRow(rs));
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return activitiesList;
    }

    /**
     * Find all activities of user with some status.
     *
     * @param user specific user
     * @param status status user request of activity
     * @return list of user activities objects
     */
    public List<UserActivity> findUserActivities(User user,int status) {
        List<UserActivity> userActivities = new ArrayList<UserActivity>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserActivityItemMapper mapper = new UserActivityItemMapper();
            pstmt = con.prepareStatement(SQL__FIND_ACCOUNT_ACTIVITIES);
            pstmt.setLong(1, user.getId());
            pstmt.setLong(2, status);
            rs = pstmt.executeQuery();

            while (rs.next())
                userActivities.add(mapper.mapRow(rs));

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return userActivities;
    }

    /**
     * Find all user requests for activity with a certain status.
     *
     * @param status status of request
     * @return list of user requests
     */
    public List<UserRequest> findUsersRequests(int status) {
        List<UserRequest> requestList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserActivityRequestMapper mapper = new UserActivityRequestMapper();
            pstmt = con.prepareStatement(SQL__FIND_ACCOUNTS_REQUESTS);
            pstmt.setLong(1, status);
            rs = pstmt.executeQuery();
            while (rs.next())
                requestList.add(mapper.mapRow(rs));
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return requestList;
    }


    /**
     * Find all user requests for activity.
     * @param user certain user
     * @return list of requests
     */
    public List<UserRequest> findUserRequests(User user) {
        List<UserRequest> requestList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserActivityRequestMapper mapper = new UserActivityRequestMapper();
            pstmt = con.prepareStatement(SQL__FIND_USER_REQUESTS);
            pstmt.setLong(1, user.getId());
            rs = pstmt.executeQuery();
            while (rs.next())
                requestList.add(mapper.mapRow(rs));
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return requestList;
    }

    /**
     * Find activities by list of their id.
     * @param ids massive of id.
     * @return list of activities
     */
    public List<Activity> findUserActivities(String[] ids) {
        List<Activity> activityItemsList = new ArrayList<Activity>();
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            ActivityItemMapper mapper = new ActivityItemMapper();

            StringBuilder query = new StringBuilder(
                    "SELECT * FROM activity WHERE id IN (");
            for (String idAsString : ids)
                query.append(idAsString).append(',');
            query.deleteCharAt(query.length() - 1);
            query.append(')');

            stmt = con.createStatement();
            rs = stmt.executeQuery(query.toString());
            while (rs.next())
                activityItemsList.add(mapper.mapRow(rs));

            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return activityItemsList;
    }


    /**
     * Locks activities to the user.
     *
     * @param user specific user
     * @param activities list of activities
     * @return true if operation success or false in other cases
     */
    public boolean setActivitiesForUser(User user,List<Activity> activities) {
        boolean res;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            long id = user.getId();

            for (Activity t : activities) {
                if(getRequestStatus(id,t.getId()) == 1 || getRequestStatus(id,t.getId()) == 2){
                    continue;
                }
                if(!addActivityForUser(con, id, t.getId(),1)){
                    return false;
                }
            }
            con.commit();
            user.setId(id);
            res = true;
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            res = false;
            ex.printStackTrace();
        }finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }


    /**
     * Help method of setActivitiesForUser.
     *
     * @param con connection
     * @param id id of user
     * @param activityId id of activity
     * @param status status of activity
     * @return
     */
    private boolean addActivityForUser(Connection con, long id, Long activityId, int status) {
        boolean result = false;
        try(PreparedStatement pstmtInsert =
                    con.prepareStatement(SET_ACTIVITY_TO_USER)) {
            int k = 1;
            pstmtInsert.setLong(k++, id);
            pstmtInsert.setLong(k++, activityId);
            pstmtInsert.setInt(k,status);

            if(pstmtInsert.executeUpdate()>0){
                result = true;
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
        return  result;
    }


    /**
     * Update activities data of the user.
     *
     * @param user specific user
     * @param requestToChange list of user activities
     * @return true if operation success or false in other cases
     */
    public boolean updateActivitiesForUser(User user, List<UserActivity> requestToChange) {
        boolean res;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            long id = user.getId();
            for (UserActivity t : requestToChange) {
                Time newTime = t.getTimeAmount();

                updateActivityForUser(con, id, t.getId(),newTime);
            }
            con.commit();
            user.setId(id);
            res = true;
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            res = false;
            ex.printStackTrace();
        }finally {

            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }


    /**
     * Help method of updateActivitiesForUser.
     *
     * @param con Connection
     * @param id id of user
     * @param activityId id of activity
     * @param newTime new value of time spent
     */
    private void updateActivityForUser(Connection con, long id, Long activityId, Time newTime) {
        try(PreparedStatement pstmt =
                    con.prepareStatement(UPDATE_ACTIVITY_TO_USER)) {
            int k = 1;
            pstmt.setString(k++,newTime.toString());
            pstmt.setLong(k++,id);
            pstmt.setLong(k,activityId);

            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
    }

    /**
     * Delete activities of the user.
     *
     * @param user specific user
     * @param deleteItems list of activities that user want to delete
     * @return true if operation success or false in other cases
     */
    public boolean deleteActivitiesForUser(User user, List<Activity> deleteItems) {
        boolean res;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            long id = user.getId();
            for (Activity t : deleteItems) {
                deleteActivityForUser(con, id, t.getId());
            }
            con.commit();
            user.setId(id);
            res = true;
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            res = false;
            ex.printStackTrace();
        }finally {

            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    /**
     * Help method of deleteActivitiesForUser.
     *
     * @param con Connection
     * @param id id of user
     * @param activityId id of activity
     */
    private void deleteActivityForUser(Connection con, long id, Long activityId) {
        try(PreparedStatement pstmt =
                    con.prepareStatement(DELETE_ACTIVITY_FROM_USER)) {
            int k = 1;
            pstmt.setLong(k++, id);
            pstmt.setLong(k, activityId);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
    }


    /**
     * Add new activity to base.
     * @param requestActivity new activity
     * @return true if operation success or false in other cases
     */
    public boolean addActivity(Activity requestActivity) {
        boolean res = false;
        Connection con = null;
        PreparedStatement pstmt;
        try{
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__ADD_ACTIVITY, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            pstmt.setString(k++, requestActivity.getName());
            pstmt.setString(k++, requestActivity.getDescription());
            pstmt.setLong(k, requestActivity.getCategoryId());

            if (pstmt.executeUpdate() > 0) {
                try(ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        requestActivity.setId(rs.getLong(1));
                        res = true;
                    }
                }
            }
            pstmt.close();
        }catch (SQLException ex) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    /**
     * Change status of visibility of activity.
     *
     * @param requestActivity specific activity
     * @param status new status of visibility
     * @return true if operation success or false in other cases
     */
    public boolean accessActivity(Activity requestActivity, String status) {
        boolean res = false;
        Connection con = null;
        PreparedStatement pstmt;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__UPDATE_ACCESS_ACTIVITY.replace("status",status));
            pstmt.setLong(1, requestActivity.getId());
            if(pstmt.executeUpdate()>0){
                res = true;
            }
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    /**
     * Find a a certain set of activities. Using for pagination.
     *
     * @param currentPage current page of table
     * @param recordsPerPage number of activities on one page
     * @param sort sort type
     * @param order order of sorting
     * @return set of sorted activities
     */
    public List<Activity> findActivities(int currentPage, int recordsPerPage,String sort,String order) {
        List<Activity> activitiesList = new ArrayList<Activity>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;

        int start = currentPage * recordsPerPage - recordsPerPage;
        try {
            con = DBManager.getInstance().getConnection();
            ActivityItemMapper mapper = new ActivityItemMapper();

            String fullRequest = SQL__FIND_ALL_ACTIVITY_SORTED.replace("sort",sort).replace("orderType",order);

            pstmt = con.prepareStatement(fullRequest);
            pstmt.setInt(1,start);
            pstmt.setInt(2, recordsPerPage);
            rs = pstmt.executeQuery();
            while (rs.next())
                activitiesList.add(mapper.mapRow(rs));

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().commitAndClose(con);
        }
        return activitiesList;
    }

    /**
     * Calculate a number of rows of activities table.
     *
     * @return amount of rows
     */
    public Integer getNumberOfRows() {
        Integer numbOfRows = 0;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__ROWS_ACTIVITY);
            rs = pstmt.executeQuery();
            if (rs.next())
                numbOfRows = rs.getInt(1);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return numbOfRows;
    }

    /**
     * Find activity by id.
     *
     * @param id specific id
     * @return activity
     */
    public Activity findActivity(long id) {
        Activity activity = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            ActivityItemMapper mapper = new ActivityItemMapper();
            pstmt = con.prepareStatement(SQL_FIND_ACTIVITY_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                activity = mapper.mapRow(rs);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return activity;
    }
    /**
     * Find activity by name.
     *
     * @param name specific name
     * @return activity
     */
    public Activity findActivity(String name) {
        Activity activity = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            ActivityItemMapper mapper = new ActivityItemMapper();
            pstmt = con.prepareStatement(SQL_FIND_ACTIVITY_BY_NAME.replace("requestName",name));
            rs = pstmt.executeQuery();
            if (rs.next())
                activity = mapper.mapRow(rs);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return activity;
    }

    /**
     * Edit an activity.
     *
     * @param requestActivity new activity
     * @return true if operation success or false in other cases
     */
    public boolean editActivity(Activity requestActivity) {
        boolean res = false;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateActivity(con, requestActivity);
            res = true;
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }


    /**
     * Return a status of user request for activity.
     *
     * @param id user id
     * @param activity_id activity id
     * @return number of status of activity request
     */
    public int getRequestStatus(Long id,Long activity_id) {
        Integer status = 0;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(FIND_REQUEST_FROM_USER);
            pstmt.setLong(1, id);
            pstmt.setLong(2, activity_id);
            rs = pstmt.executeQuery();
            if (rs.next())
                status = rs.getInt(1);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return status;
    }

    /**
     * Update an activity.
     *
     * @param con connection
     * @param activity activity to update
     * @throws SQLException
     */
    public void updateActivity(Connection con, Activity activity) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_ACCESS_ACTIVITY);
        int k = 1;
        pstmt.setString(k++, activity.getName());
        pstmt.setString(k++, activity.getDescription());
        pstmt.setLong(k++, activity.getCategoryId());
        pstmt.setLong(k, activity.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Confirmed a user requests for adding or deleting activities from their list.
     *
     * @param confirm list of requests
     * @param operation add or delete
     * @return true if operation success or false in other cases
     */
    public boolean confirmRequests(ArrayList<UserRequest> confirm,String operation) {
        boolean res;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            for (UserRequest t : confirm) {
                if(operation.equals("add")){
                    if(!confirmActivityForUser(con, t.getAccount_id(), t.getActivity_id(),2))
                        return false;
                }else if(operation.equals("delete")){
                    if(!deleteRequestForUser(con, t.getAccount_id(), t.getActivity_id()))
                        return false;
                }
            }
            con.commit();
            res = true;
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            res = false;
            ex.printStackTrace();
        }finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }



    /**
     *  Help method for confirmRequests.
     *  Confirmed a user request for adding or deleting.
     *
     * @param con connection
     * @param id user id
     * @param activityId activity id
     * @param status new status of activity
     * @return true if operation success or false in other cases
     */
    private boolean confirmActivityForUser(Connection con, long id, Long activityId, int status) {
        boolean result = false;
        try(PreparedStatement pstmtInsert =
                    con.prepareStatement(UPDATE_ACTIVITY_ACCESS_TO_USER);
            PreparedStatement pstmtUpdate =
                    con.prepareStatement(SQL__CONFIRM_REQUEST_FROM_USER)) {
            int k = 1;
            pstmtInsert.setInt(k++,status);
            pstmtInsert.setLong(k++, id);
            pstmtInsert.setLong(k, activityId);

            pstmtUpdate.setLong(1,activityId);
            if(pstmtInsert.executeUpdate()>0 && pstmtUpdate.executeUpdate()>0){
                result = true;
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
        return  result;
    }

    /**
     * Canceled a user requests for adding or deleting activities from their list.
     *
     * @param confirm list of requests
     * @param operation add or delete
     * @return true if operation success or false in other cases
     */
    public boolean cancelRequests(ArrayList<UserRequest> confirm, String operation) {
        boolean res;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);


            for (UserRequest t : confirm) {
                if(operation.equals("add")){
                    if(!cancelRequestForUser(con, t.getAccount_id(), t.getActivity_id())){
                        return false;
                    }
                }else if(operation.equals("delete")){
                    if(!confirmActivityForUser(con, t.getAccount_id(), t.getActivity_id(),2))
                        return false;
                }
            }
            con.commit();
            res = true;
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            res = false;
            ex.printStackTrace();
        }finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    /**
     *  Help method for cancelRequests.
     *  Canceled a user request for adding or deleting.
     *
     * @param con connection
     * @param id user id
     * @param activityId activity id
     * @return true if operation success or false in other cases
     */
    private boolean cancelRequestForUser(Connection con, long id, Long activityId) {
        boolean result = false;
        try(PreparedStatement pstmtInsert =
                    con.prepareStatement(CANCEL_REQUEST_FROM_USER)) {
            int k = 1;
            pstmtInsert.setLong(k++, id);
            pstmtInsert.setLong(k, activityId);
            if(pstmtInsert.executeUpdate()>0){
                result = true;
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
        return  result;
    }


    /**
     * Delete an activity from user list and decrease amount of users, that have this activity in their list.
     *
     * @param con connection
     * @param id user id
     * @param activityId activity id
     * @return true if operation success or false in other cases
     */
    private boolean deleteRequestForUser(Connection con, long id, Long activityId) {
        boolean result = false;
        try(PreparedStatement pstmtInsert =
                    con.prepareStatement(DELETE_REQUEST_FROM_USER);
            PreparedStatement pstmtUpdate =
                    con.prepareStatement(SQL__DELETE_REQUEST_FROM_USER)) {
            int k = 1;
            pstmtInsert.setLong(k++, id);
            pstmtInsert.setLong(k, activityId);

            pstmtUpdate.setLong(1,activityId);
            if(pstmtInsert.executeUpdate()>0 && pstmtUpdate.executeUpdate()>0){
                result = true;
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        }
        return  result;
    }

    public List<Status> findStatusesInformation() {
        List<Status> statusesList = new ArrayList<>();
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL__FIND_ALL_STATUS_ITEMS);
            while (rs.next()) {
                Status status = new Status();

                status.setId(rs.getLong(Fields.ENTITY__ID));
                status.setName(rs.getString(Fields.NAME));
                status.setDescription(rs.getString(Fields.DESCRIPTION));
                statusesList.add(status);
            }
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return statusesList;
    }


    /**
     * Extracts a activity from the result set row.
     */

    private static class ActivityItemMapper implements EntityMapper<Activity> {
        @Override
        public Activity mapRow(ResultSet rs) {
            try {
                Activity activity = new Activity();

                activity.setId(rs.getLong(Fields.ENTITY__ID));
                activity.setName(rs.getString(Fields.NAME));
                activity.setDescription(rs.getString(Fields.DESCRIPTION));
                activity.setUserAmount(rs.getInt(Fields.ACTIVITY__USER_AMOUNT));
                activity.setCategoryId(rs.getLong(Fields.ACTIVITY__CATEGORY_ID));
                activity.setAccess(rs.getString(Fields.ACCESS));

                return activity;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }


    /**
     * Extracts a user activity from the result set row.
     */
    private static class UserActivityItemMapper implements EntityMapper<UserActivity> {

        @Override
        public UserActivity mapRow(ResultSet rs) {
            try {
                UserActivity userActivity = new UserActivity();

                userActivity.setId(rs.getLong(Fields.ENTITY__ID));
                userActivity.setName(rs.getString(Fields.NAME));
                userActivity.setTimeAmount(rs.getTime(Fields.ACTIVITY__TIME_AMOUNT));
                userActivity.setStatusId(rs.getLong(Fields.ACTIVITY__STATUS_ID));
                userActivity.setCategoryId(rs.getInt(Fields.ACTIVITY__CATEGORY_ID));

                return userActivity;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Extracts a user request for activity from the result set row.
     */
    private static class UserActivityRequestMapper implements EntityMapper<UserRequest> {

        @Override
        public UserRequest mapRow(ResultSet rs) {
            try {
                UserRequest userRequest = new UserRequest();

                userRequest.setAccount_id(rs.getLong(Fields.ACTIVITY__ACCOUNT_ID));
                userRequest.setActivity_id(rs.getLong(Fields.ACTIVITY__ACTIVITY_ID));
                userRequest.setName(rs.getString(Fields.NAME));

                userRequest.setLogin(rs.getString(Fields.USER__LOGIN));
                userRequest.setStatusId(rs.getLong(Fields.ACTIVITY__STATUS_ID));
                userRequest.setStatusName(rs.getString(Fields.ACTIVITY__STATUS_NAME));

                return userRequest;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}
