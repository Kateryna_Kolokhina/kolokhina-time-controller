package ua.kolokhina.timeController.db;

import ua.kolokhina.timeController.Fields;
import ua.kolokhina.timeController.db.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Category entity.
 */
public class CategoryDao {

    private DBManager dbManager;

    public CategoryDao(){
        dbManager = DBManager.getInstance();
    }
    CategoryDao(boolean test){
        dbManager = DBManager.getInstance(test);
    }
    private static final String SQL__FIND_ALL_CATEGORIES =
            "SELECT * FROM category order by category_id";

    private static final String SQL__FIND_CATEGORY_BY_ID =
            "SELECT * from category where category_id = ?";
    private static final String SQL__FIND_CATEGORY_BY_NAME =
            "SELECT * from category where name = 'requestName'";

    private static final String SQL__DELETE_CATEGORY =
            "DELETE FROM category where category_id = ?";

    private static final String SQL__UPDATE_CATEGORY = "UPDATE category SET name =?, description=? WHERE category_id=?";
    private static final String SQL__FIND_CATEGORIES_PER_PAGE = "SELECT * from category order by category_id LIMIT ?,?";
    private static final String SQL__ROWS_OF_CATEGORY = "SELECT COUNT(category_id) from category";
    private static final String SQL__ADD_CATEGORY = "INSERT INTO category (name,description) VALUES (?,?)";

    /**
     * Returns all categories.
     *
     * @return List of category entities.
     */
    public List<Category> findCategories( int currentPage, int recordsPerPage) {
        List<Category> categoriesList = new ArrayList<Category>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        int start = currentPage * recordsPerPage - recordsPerPage;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            pstmt = con.prepareStatement(SQL__FIND_CATEGORIES_PER_PAGE);
            pstmt.setInt(1,start);
            pstmt.setInt(2, recordsPerPage);
            rs = pstmt.executeQuery();
            while (rs.next())
                categoriesList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return categoriesList;
    }

    public List<Category> findCategories() {

        List<Category> categoriesList = new ArrayList<Category>();
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL__FIND_ALL_CATEGORIES);
            while (rs.next())
                categoriesList.add(mapper.mapRow(rs));
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return categoriesList;
    }

    public Category findCategory(Long id) {
        Category category = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            pstmt = con.prepareStatement(SQL__FIND_CATEGORY_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                category = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return category;
    }

    public Category findCategory(String name) {
        Category category = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            pstmt = con.prepareStatement(SQL__FIND_CATEGORY_BY_NAME.replace("requestName",name));
            rs = pstmt.executeQuery();
            if (rs.next())
                category = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return category;
    }
    public int getNumberOfRowsCategory() {
        Integer numbOfRows = 0;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL__ROWS_OF_CATEGORY);
            rs = pstmt.executeQuery();
            if (rs.next())
                numbOfRows = rs.getInt(1);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return numbOfRows;
    }

    public boolean addCategory(Category category) {
        boolean res = false;
        Connection con = null;
        PreparedStatement pstmt;
        try{
            con = DBManager.getInstance().getConnection();
            pstmt =
                    con.prepareStatement(SQL__ADD_CATEGORY, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            pstmt.setString(k++, category.getName());
            pstmt.setString(k, category.getDescription());

            if (pstmt.executeUpdate() > 0) {
                try(ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        category.setId(rs.getLong(1));
                        res = true;
                    }
                }
            }
            pstmt.close();
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        }finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    public boolean editCategory(Category requestCategory) {
       boolean res = false;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateCategory(con, requestCategory);
            res = true;
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    public boolean deleteCategory(Category requestCategory) {
        boolean res = false;
        Connection con = null;
        PreparedStatement pstmt ;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL__DELETE_CATEGORY);
            pstmt.setLong(1, requestCategory.getId());
            if(pstmt.executeUpdate()>0){
                res = true;
            }
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }


    // //////////////////////////////////////////////////////////
    // Entity access methods (for transactions)
    // //////////////////////////////////////////////////////////


    public void updateCategory(Connection con, Category category) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL__UPDATE_CATEGORY);
        int k = 1;
        pstmt.setString(k++, category.getName());
        pstmt.setString(k++, category.getDescription());
        pstmt.setLong(k, category.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }


    /**
     * Extracts a category from the result set row.
     */
    private static class CategoryMapper implements EntityMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs) {
            try {
                Category category = new Category();
                category.setId(rs.getLong(Fields.CATEGORY__ID));
                category.setName(rs.getString(Fields.NAME));
                category.setDescription(rs.getString(Fields.DESCRIPTION));
                return category;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
