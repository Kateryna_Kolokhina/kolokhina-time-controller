package ua.kolokhina.timeController.db;

import ua.kolokhina.timeController.Fields;
import ua.kolokhina.timeController.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for User entity.
 */
public class UserDao {
    private DBManager dbManager;
    private static final String SQL_ADD_NEW_ACCOUNT =
            "INSERT INTO account (login,email,password,role_id) VALUES (?,?,?,?)";

    private static final String SQL__FIND_ACCOUNT_BY_LOGIN =
            "SELECT * FROM account WHERE login=?";

    private static final String SQL__FIND_ACCOUNT_BY_ID =
            "SELECT * FROM account WHERE id=?";

    private static final String SQL_ACCOUNT_USER =
            "UPDATE account SET login =?, password=?, email=? WHERE id=?";

    private static final String SQL_ACCOUNT_SET_ACCESS =
            "UPDATE account SET access='accessType' WHERE id=?";
    private static final String SQL__FIND_USERS_PER_PAGE = "SELECT * FROM account WHERE role_id = 1 LIMIT ?, ?";
    private static final String SQL_ROWS_USERS = "SELECT COUNT(id) from account WHERE role_id = 1";

    public UserDao(){
        dbManager = DBManager.getInstance();
    }

    public UserDao(boolean test){
        dbManager = DBManager.getInstance(test);
    }

    public boolean addUser(User user) {
        boolean res = false;
        Connection con = null;
        PreparedStatement pstmt = null;
        try{
            con = DBManager.getInstance().getConnection();
            pstmt =
                    con.prepareStatement(SQL_ADD_NEW_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            pstmt.setString(k++, user.getLogin());
            pstmt.setString(k++, user.getEmail());
            pstmt.setString(k++, user.getPassword());
            pstmt.setInt(k, user.getRole());

            if (pstmt.executeUpdate() > 0) {
                try(ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        user.setId(rs.getLong(1));
                        res = true;
                    }
                }
            }
            pstmt.close();
        }catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        }finally {
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }
    /**
     * Returns a user with the given identifier.
     *
     * @param id
     *            User identifier.
     * @return User entity.
     */
    public User findUser(Long id) {
        User user = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserMapper mapper = new UserMapper();
            pstmt = con.prepareStatement(SQL__FIND_ACCOUNT_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                user = mapper.mapRow(rs);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return user;
    }

    public List<User> findUsers(int currentPage, int recordsPerPage)  {
        List<User> usersList = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        int start = currentPage * recordsPerPage - recordsPerPage;
        try {
            con = DBManager.getInstance().getConnection();
            UserDao.UserMapper mapper = new  UserDao.UserMapper();
            pstmt = con.prepareStatement(SQL__FIND_USERS_PER_PAGE);
            pstmt.setInt(1,start);
            pstmt.setInt(2, recordsPerPage);
            rs = pstmt.executeQuery();

            while (rs.next())
                usersList.add(mapper.mapRow(rs));

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return usersList;
    }

    public Integer getNumberOfRows() {
        int numbOfRows = 0;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_ROWS_USERS);
            rs = pstmt.executeQuery();
            if (rs.next())
                numbOfRows = rs.getInt(1);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return numbOfRows;
    }

    /**
     * Returns a user with the given login.
     *
     * @param login
     *            User login.
     * @return User entity.
     */

    public User findUserByLogin(String login) {
        User user = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            UserMapper mapper = new UserMapper();
            pstmt = con.prepareStatement(SQL__FIND_ACCOUNT_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();

            if (rs.next())
                user = mapper.mapRow(rs);

            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return user;
    }

    /**
     * Update user.
     *
     * @param user
     *            user to update.
     */
    public void updateUser(User user) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateUser(con, user);
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
    }

    // //////////////////////////////////////////////////////////
    // Entity access methods (for transactions)
    // //////////////////////////////////////////////////////////

    /**
     * Update user.
     *
     * @param user
     *            user to update.
     * @throws SQLException
     */
    public void updateUser(Connection con, User user) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_ACCOUNT_USER);
        int k = 1;
        pstmt.setString(k++, user.getPassword());
        pstmt.setLong(k, user.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }


    public boolean setAccessUser(User user, String access) {
        boolean res = false;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            setAccessUser(con, user,access);
            res = true;
        } catch (SQLException ex) {
            if (con != null) {
                DBManager.getInstance().rollbackAndClose(con);
            }
            ex.printStackTrace();
        } finally {
            if (con != null) {
                DBManager.getInstance().commitAndClose(con);
            }
        }
        return res;
    }

    /**
     * Update user.
     *
     * @param user
     *            user to update.
     * @throws SQLException
     */
    public void setAccessUser(Connection con, User user,String access) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_ACCOUNT_SET_ACCESS.replace("accessType",access));
        pstmt.setLong(1, user.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Extracts a user from the result set row.
     */
    private static class UserMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getLong(Fields.ENTITY__ID));
                user.setLogin(rs.getString(Fields.USER__LOGIN));
                user.setPassword(rs.getString(Fields.USER__PASSWORD));
                user.setEmail(rs.getString(Fields.USER__EMAIL));
                user.setCreateDate(rs.getString(Fields.USER__CREATE_TIME));
                user.setLocaleName(rs.getString(Fields.USER__LOGIN));
                user.setRole(rs.getInt(Fields.USER__ROLE_ID));
                user.setAccess(rs.getString(Fields.ACCESS));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
