package ua.kolokhina.timeController.db.entity;

import java.util.Objects;


/**
 * User entity.
 *
 * @author  K.Kolokhina
 */
public class Activity extends Entity {

	private static final long serialVersionUID = 4716395168539434663L;

	private String name;
	private Integer userAmount;
	private String description;
	private Long categoryId;
	private String access;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUserAmount() {
		return userAmount;
	}

	public void setUserAmount(Integer userAmount) {
		this.userAmount = userAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Activity activity = (Activity) o;
		return name.equals(activity.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, description, categoryId);
	}

	@Override
	public String toString() {
		return "Activity{" +
				"name='" + name + '\'' +
				", userAmount=" + userAmount +
				", description='" + description + '\'' +
				", categoryId=" + categoryId +
				'}';
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}
}