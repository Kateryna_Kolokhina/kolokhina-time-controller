package ua.kolokhina.timeController.db.entity;

import java.util.Objects;

/**
 * Category entity.
 * 
 * @author K.Kolokhina
 * 
 */
public class Category extends Entity {

	private static final long serialVersionUID = 2386302708905518585L;

	private String name;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Category [name=" + name + ", getId()=" + getId() + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Category category = (Category) o;
		return Objects.equals(name, category.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, description);
	}
}
