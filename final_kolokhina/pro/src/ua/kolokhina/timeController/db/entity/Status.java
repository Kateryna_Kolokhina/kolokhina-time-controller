package ua.kolokhina.timeController.db.entity;

import java.util.Objects;

/**
 * Status entity.
 * 
 * @author K.Kolokhina
 * 
 */
public class Status extends Entity {

	private static final long serialVersionUID = 2386302708905518585L;

	private String name;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Category [name=" + name + ", getId()=" + getId() + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, description);
	}
}
