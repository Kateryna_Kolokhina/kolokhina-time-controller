package ua.kolokhina.timeController.db.entity;

import java.util.Objects;

/**
 * User entity.
 *
 * @author K.Kolokhina
 *
 */
public class User extends Entity {

	private static final long serialVersionUID = -6889036256149495388L;

	private String login;
	private String email;
	private String localeName;
	private String password;
	private String createDate;
	private int role;
	private String access;

	public static User createUser(String login, String password,int role) {
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		// java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		user.setRole(role);
		return user;
	}

	public static User createUser(String login, String email, String password,int role) {
		User user = new User();
		user.setLogin(login);
		user.setEmail(email);
		user.setPassword(password);
		// java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		user.setRole(role);
		return user;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}


	@Override
	public String toString() {
		return "User{" +
				"login='" + login + '\'' +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", role=" + role +
				", active=" + access +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		if(!Objects.equals(login, user.login) || !Objects.equals(email, user.email))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getAccess() { return access; }

	public void setAccess(String access) {
		this.access = access;
	}

	public String getLocaleName() {
		return localeName;
	}

	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}

	public void validate(){

	}

}
