package ua.kolokhina.timeController.db.entity;

import java.sql.Time;

/**
 * Activity of User entity.
 * 
 * @author K.Kolokhina
 * 
 */
public class UserActivity extends Entity {

	private static final long serialVersionUID = 4716395168539434663L;

	private String name;
	private Time timeAmount;
	private Long statusId;
	private Integer categoryId;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Time getTimeAmount() {
		return timeAmount;
	}

	public void setTimeAmount(Time timeAmount) {
		this.timeAmount = timeAmount;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	@Override
	public String toString() {
		return "UserActivity{" +
				"name='" + name + '\'' +
				", timeAmount=" + timeAmount +
				", statusId=" + statusId +
				'}';
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
}