package ua.kolokhina.timeController.db.entity;


/**
 * Request of User entity.
 * 
 * @author  K.Kolokhina
 * 
 */
public class UserRequest {

	private static final long serialVersionUID = 4716395168539434663L;

	private Long account_id;
	private Long activity_id;
	private String login;
	private String name;
	private Long statusId;
	private String statusName;

	@Override
	public String toString() {
		return "UserRequest{" +
				"account_id=" + account_id +
				", activity_id=" + activity_id +
				", login='" + login + '\'' +
				", name='" + name + '\'' +
				", statusId=" + statusId +
				'}';
	}

	public Long getAccount_id() {
		return account_id;
	}

	public void setAccount_id(Long account_id) {
		this.account_id = account_id;
	}

	public Long getActivity_id() {
		return activity_id;
	}

	public void setActivity_id(Long activity_id) {
		this.activity_id = activity_id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}


	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}