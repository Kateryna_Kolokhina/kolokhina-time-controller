package ua.kolokhina.timeController.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.web.command.Command;
import ua.kolokhina.timeController.web.command.CommandContainer;

/**
 * Main servlet controller.
 * 
 * @author K.Kolokhina
 * 
 */
public class Controller extends HttpServlet {
	
	private static final long serialVersionUID = 2423353715955164816L;

	private static final Logger log = Logger.getLogger(Controller.class);

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Main method of this controller.
	 */
	private void process(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		log.debug("Controller starts");

		// extract command name from the request
		String commandName = request.getParameter("command");
		log.trace("Request parameter: command --> " + commandName);

		// obtain command object by its name
		Command command = CommandContainer.get(commandName);
		log.trace("Obtained command --> " + command);

		String redirectAdress = command.execute(request, response);

		if((command.isRedirect() && !redirectAdress.equals(Path.PAGE__ERROR_PAGE))){

			//if we on redirect page and need to go to another without new data
			// execute command and get forward address
			log.trace("Redirect address --> " + redirectAdress);

			log.debug("Controller finished, now go to redirect address --> " + redirectAdress);

			// if the redirect address is not null go to the address
			if (redirectAdress != null) {
				response.sendRedirect(request.getContextPath() + redirectAdress);
			}
		}else {
			// execute command and get forward address
			log.trace("Forward address --> " + redirectAdress);

			log.debug("Controller finished, now go to forward address --> " + redirectAdress);

			// if the forward address is not null go to the address
			if (redirectAdress != null) {
				RequestDispatcher disp = request.getRequestDispatcher(redirectAdress);
				disp.forward(request, response);
			}
		}
	}

}