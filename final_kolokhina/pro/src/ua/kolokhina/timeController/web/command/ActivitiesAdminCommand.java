package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;

import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ActivitiesAdminCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger log = Logger.getLogger(ActivitiesAdminCommand.class);

	private String errorMessage;
	String forward = Path.PAGE__ERROR_PAGE;

	public ActivitiesAdminCommand(boolean redirect) {
		super(redirect);
	}

    public ActivitiesAdminCommand() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
		log.debug("Command starts");
		int currentPage = 0;
		int recordsPerPage = 0;

		String update = request.getParameter("update");
		log.trace("Request parameter: update --> " + update);

		String activityId = request.getParameter("activityId");
		log.trace("Request parameter: activityId --> " + activityId);

		if( update != null && update.equals("update")){
			long id;

			if (activityId == null || activityId.isEmpty()) {
				errorMessage = "Id is null";
				request.setAttribute("errorMessage", errorMessage);
				log.error("errorMessage --> " + errorMessage);
				log.trace("Set the attribute: forward --> " + forward);
				return forward;
			} else {
				id = Long.parseLong(activityId);
			}
			log.info("Get the activity attribute: id --> " + id);

			Activity requestActivity = new ActivitiesDao().findActivity(id);
			log.trace("Found in DB: requestActivity --> " + requestActivity);

			if (requestActivity == null) {
				errorMessage = "This activity is not exist";
				request.setAttribute("errorMessage", errorMessage);
				log.error("errorMessage --> " + errorMessage);
				return forward;
			}

			String access = requestActivity.getAccess();
			log.info("Request parameter: access --> " + access);

			if(!validateAccess(access)){
				request.setAttribute("errorMessage", errorMessage);
				return forward;
			}

			if(access.equals("enable"))
				access = "disable";
			else if(access.equals("disable"))
				access = "enable";
			boolean result = new ActivitiesDao().accessActivity(requestActivity,access);

			if (!result) {
				request.getSession().setAttribute("getAlert", "updateError");
			} else {
				request.getSession().setAttribute("getAlert", "success");
			}
		}

		String currentPageString = request.getParameter("currentPage");
		log.trace("Request parameter: currentPage --> " + currentPage);

		String recordsPerPageString = request.getParameter("recordsPerPage");
		log.trace("Request parameter: recordsPerPage --> " + recordsPerPage);

		if(currentPageString == null || currentPageString.isEmpty() ||
		recordsPerPageString == null || recordsPerPageString.isEmpty()){
			currentPage = 1;
			recordsPerPage = 5;
		}else {
			currentPage = Integer.parseInt(currentPageString);
			recordsPerPage = Integer.parseInt(recordsPerPageString);
		}

		String sort_type = request.getParameter("sort");
		log.trace("Request parameter: sort --> " + sort_type);

		if(sort_type == null || sort_type.isEmpty()) {
			sort_type = "id";
		}else if(sort_type.equals("name") || sort_type.equals("user_amount") ||
				sort_type.equals("category_id") || sort_type.equals("id") ){
			request.setAttribute("sort",sort_type);
			log.trace("Set the request attribute: sort --> " + sort_type);
		}else{
			errorMessage = "No such type of sorting";
			request.setAttribute("errorMessage", errorMessage);
			log.error("errorMessage --> " + errorMessage);
			return forward;
		}

		request.setAttribute("sort", sort_type);
		log.trace("Set the request attribute: sort --> " + sort_type);

		String order = request.getParameter("order");
		log.trace("Request parameter: sorting order --> " + order);

		if(order == null || order.isEmpty() || order.equals("ascend"))
			order = "";
		else if(order.equals("descend")){
			order = "DESC";
		}

		request.setAttribute("order",order);
		log.trace("Set the request attribute: order --> " + order);

		String category = request.getParameter("category");
		log.trace("Request parameter: category --> " + category);

		List<Category> listCategories = new CategoryDao().findCategories();
		log.trace("Found in DB: listCategories --> " + listCategories);
		boolean exist = false;

		if(category == null || category.isEmpty()){
			category = "";
		}else{
			for (Category cat: listCategories) {
				if(cat.getId().equals(Long.valueOf(category))) {
					exist = true;
					break;
				}
			}
			if(!exist){
				errorMessage = "No such type of sorting";
				request.setAttribute("errorMessage", errorMessage);
				log.error("errorMessage --> " + errorMessage);
				return forward;
			}
		}

		request.setAttribute("category", category);
		log.trace("Set the request attribute: category --> " + category);

		request.setAttribute("listCategories", listCategories);
		log.trace("Set the request attribute: listCategories --> " + listCategories);

		List<Activity> activitiesList = new ActivitiesDao().findActivities(currentPage,
				recordsPerPage,sort_type,order);

		log.trace("Found in DB: activitiesList --> " + activitiesList);

		request.setAttribute("activitiesList", activitiesList);
		log.trace("Set the request attribute: activitiesList --> " + activitiesList);

		int rows = new ActivitiesDao().getNumberOfRows();

		int nOfPages = rows / recordsPerPage;

		if (rows % recordsPerPage > 0) {
			nOfPages++;
		}
		request.setAttribute("noOfPages", nOfPages);
		log.trace("Set the request attribute: noOfPages --> " + nOfPages);

		request.setAttribute("currentPage", currentPage);
		log.trace("Set the request attribute: currentPage --> " + currentPage);

		request.setAttribute("recordsPerPage", recordsPerPage);
		log.trace("Set the request attribute: recordsPerPage --> " + recordsPerPage);

		forward = Path.PAGE__ADMIN_LIST_ACTIVITIES;

		log.debug("Command finished");
		log.trace("Set the attribute: forward --> " + forward);
		return forward;
	}

	public boolean validateAccess(String access){
		if (access == null || access.isEmpty() ||
				( !access.equals("enable") && !access.equals("disable") ) ){
			errorMessage = "Input data is not correct";
			log.error("errorMessage --> " + errorMessage);
			return false;
		}
		return true;
	}

}