package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class AddActivitiesCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(AddActivitiesCommand.class);

    public AddActivitiesCommand(boolean redirect) {
        super(redirect);
    }

    public AddActivitiesCommand() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Command starts");

        User user = (User)request.getSession().getAttribute("user");
        log.trace("Get the session attribute: user --> " + user);

        User actualUserInformation = new UserDao().findUserByLogin(user.getLogin());
        log.trace("Found in DB: user  --> " + actualUserInformation);

        String access = user.getAccess();

        if(access.equals("disable")){
            String errorMessage = "You are not allowed to this action";
            request.setAttribute("errorMessage",errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return Path.PAGE__ERROR_PAGE;
        }

        if (request.getParameter("add") != null && !request.getParameter("add").isEmpty()) {
            return addActivities(request, response);
        }
        return Path.PAGE__ERROR_PAGE;
    }

    private String addActivities(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Command add starts");

        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;
        User user = (User)request.getSession().getAttribute("user");
        log.trace("Get the session attribute: user --> " + user);
        
        String[] selectedId = request.getParameterValues("itemId");
        log.trace("Request parameter: itemId --> " + Arrays.toString(selectedId));

        if (selectedId == null || selectedId.length == 0){
            errorMessage = "Selected nothing";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {
            log.info("User select activities with id --> " + Arrays.toString(selectedId));

            List<Activity> activitiesItems;
            activitiesItems = new ActivitiesDao().findUserActivities(selectedId);
//            List<UserActivity> userActivities = new ActivitiesDao().findUserActivities(user,2);
            log.trace("Found in DB: activitiesItems --> " + activitiesItems.size());
            boolean result = new ActivitiesDao().setActivitiesForUser(user, activitiesItems);
            log.info("result --> " + result);
            if(!result){
                errorMessage = "Success. But you already make a request for some of this activities. Please wait until admin check them";
                request.getSession().setAttribute("getAlert", "addError");
                request.getSession().setAttribute("errorMessage", errorMessage);
            }else{
                request.getSession().setAttribute("getAlert", "add");
            }

        }

        forward = Path.COMMAND__LIST_ACTIVITIES;
        log.debug("Command finished");
        log.trace("Set the  attribute: forward --> " + forward);
        return forward;
    }

}
