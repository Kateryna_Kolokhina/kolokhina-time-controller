package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;

import ua.kolokhina.timeController.db.entity.Activity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddActivityCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(AddActivityCommand.class);

    private String errorMessage;
    private String forward = Path.PAGE__ERROR_PAGE;

    public AddActivityCommand(){}
    public AddActivityCommand(boolean redirect) {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){

        log.debug("Command starts");
        String name = request.getParameter("name");
        log.trace("Request parameter: name --> " + name);

        String description = request.getParameter("description");
        log.trace("Request parameter: description --> " + description);

        String category = request.getParameter("category");
        log.trace("Request parameter: category --> " + category);

        if(!validateData(name,description,category)){
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        Activity requestActivity = new Activity();
        requestActivity.setName(name);
        requestActivity.setDescription(description);
        requestActivity.setCategoryId(Long.valueOf(category));

        boolean result = new ActivitiesDao().addActivity(requestActivity);
        log.info("result --> " + result);

        if(!result){
            errorMessage = "Activity with this name is already exist or Chosen category isn't exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else{
            request.getSession().setAttribute("getAlert", "success");
        }

        forward = Path.PAGE__ADMIN_ADD_ACTIVITY;
        log.debug("Command finished");
        log.trace("Set the  attribute: forward --> " + forward);
        return forward;
    }
    public boolean validateData(String name,String description,String category){
        if (name == null || description == null || category == null ||
                name.isEmpty() || description.isEmpty() || category.isEmpty()) {
            errorMessage = "Input data cannot be empty";
            log.error("errorMessage --> " + errorMessage);
            return false;
        }
        return true;
    }
}
