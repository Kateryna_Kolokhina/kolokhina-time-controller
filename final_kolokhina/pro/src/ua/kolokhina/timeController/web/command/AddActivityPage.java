package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddActivityPage extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger log = Logger.getLogger(AddActivityPage.class);

	public AddActivityPage(boolean redirect) {
		super(redirect);
	}

    public AddActivityPage() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){

		log.debug("Command start");
		List<Category> listCategories = new CategoryDao().findCategories();
		log.trace("Found in DB: listCategories --> " + listCategories);

		request.setAttribute("listCategories", listCategories);

		log.trace("Set the request attribute: listCategories --> " + listCategories);
		log.debug("Command finished");
		return Path.PAGE__ADMIN_ADD_ACTIVITY;
	}

}