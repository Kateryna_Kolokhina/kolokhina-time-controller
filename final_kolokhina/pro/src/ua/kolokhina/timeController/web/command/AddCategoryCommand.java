package ua.kolokhina.timeController.web.command;


import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddCategoryCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(AddCategoryCommand.class);
    private String errorMessage;
    private String forward = Path.PAGE__ERROR_PAGE;


    public AddCategoryCommand(){}
    public AddCategoryCommand(boolean redirect) {
        super(redirect);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");

        String name = request.getParameter("name");
        log.trace("Request parameter: name --> " + name);

        String description = request.getParameter("description");
        log.trace("Request parameter: description --> " + description);


        if (!validateData(name,description)) {
            request.setAttribute("errorMessage", errorMessage);
            return Path.PAGE__ERROR_PAGE;
        }

        Category requestCategory = new Category();
        requestCategory.setName(name);
        requestCategory.setDescription(description);

        List<Category> categories = new CategoryDao().findCategories();
        log.trace("Found in DB: categories --> " + categories);

        if(categories.contains(requestCategory)){
            errorMessage = "This category is already exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        boolean result = new CategoryDao().addCategory(requestCategory);

        log.info("result --> " + result);

        if(!result){
            request.getSession().setAttribute("getAlert", "addError");
            return forward;
        }else{
            request.getSession().setAttribute("getAlert", "success");
        }
        forward = Path.PAGE__ADMIN_ADD_CATEGORY;
        log.debug("Command finished");
        log.trace("Set the  attribute: forward --> " + forward);
        return forward;
    }

    public boolean validateData(String name,String description){
        if (name == null || description == null ||
                name.isEmpty() || description.isEmpty()) {
            errorMessage = "Input data cannot be empty";
            log.error("errorMessage --> " + errorMessage);
            return false;
        }
        return true;
    }
}
