package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddCategoryPage extends Command {

    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(AddCategoryPage.class);

    AddCategoryPage(){}
    AddCategoryPage(boolean redirect){
            super(redirect);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command start");

        log.debug("Command finished");
        log.trace("Set the  attribute: forward --> " + Path.PAGE__ADMIN_ADD_CATEGORY);
        return Path.PAGE__ADMIN_ADD_CATEGORY;
    }
}
