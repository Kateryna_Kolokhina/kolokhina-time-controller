package ua.kolokhina.timeController.web.command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;


/**
 * Holder for all commands.
 * 
 * @author K.Kolokhina
 * 
 */
public class CommandContainer {
	
	private static final Logger log = Logger.getLogger(CommandContainer.class);
	
	private static Map<String, Command> commands = new TreeMap<String, Command>();

	static {
		// common commands
		commands.put("login", new LoginCommand(true));
		commands.put("registration", new RegistrationCommand(true));
		commands.put("logout", new LogoutCommand(true));
		commands.put("noCommand", new NoCommand());

		commands.put("test", new ViewSettingsCommand());
		commands.put("viewSettings", new ViewSettingsCommand());
		commands.put("updateSettings", new UpdateSettingsCommand(true));

		// client commands
		commands.put("listActivities", new ListActivitiesCommand());

		commands.put("addActivities", new AddActivitiesCommand(true));
		commands.put("userActivities", new UserActivitiesCommand());
		commands.put("editActivities", new EditActivitiesCommand(true));

		// admin commands
		commands.put("listAccounts", new ListAccountsCommand());
		commands.put("updateAccount", new UpdateAccountCommand(true));
		commands.put("viewAccount", new ViewAccountCommand());

		commands.put("editCategory", new EditCategoryPage());
		commands.put("editCategoryCommand", new EditCategoryCommand(true));

		commands.put("addCategory", new AddCategoryPage());
		commands.put("addCategoryCommand", new AddCategoryCommand(true));

		commands.put("deleteCategory", new DeleteCategoryCommand(true));
		commands.put("listCategories", new ListCategoriesCommand());

		commands.put("activitiesAdmin",new ActivitiesAdminCommand());

		commands.put("addActivityCommand",new AddActivityCommand(true));
		commands.put("addActivity",new AddActivityPage());


		commands.put("editActivity", new EditActivityPage());
		commands.put("editActivityCommand", new EditActivityCommand(true));


		commands.put("requests",new RequestsPage());
		//TODO add request command
		commands.put("editRequestsCommand",new EditRequestsCommand(true));

		log.debug("Command container was successfully initialized");
		log.trace("Number of commands --> " + commands.size());
	}

	/**
	 * Returns command object with the given name.
	 * 
	 * @param commandName
	 *            Name of the command.
	 * @return Command object.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			log.trace("Command not found, name --> " + commandName);
			return commands.get("noCommand"); 
		}
		
		return commands.get(commandName);
	}
	
}