package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;

import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Category;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteCategoryCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(DeleteCategoryCommand.class);

    public DeleteCategoryCommand(boolean redirect) {
        super(redirect);
    }

    public DeleteCategoryCommand() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Command starts");
        // error handler
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        String idString = request.getParameter("id");
        long id = 0;

        if (idString == null || idString.isEmpty()) {
            errorMessage = "Id is null";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        } else {
            try {
                id = Long.parseLong(request.getParameter("id"));
            }catch (NumberFormatException ex){
                    errorMessage = "Id is non valid";
                    request.setAttribute("errorMessage", errorMessage);
                    log.error("errorMessage --> " + errorMessage);
                    return forward;
                }
        }
        log.trace("Request parameter: id --> " + id);

        Category requestCategory = new CategoryDao().findCategory(id);

        log.trace("Found in DB: requestCategory --> " + requestCategory);

        if (requestCategory == null) {
            errorMessage = "This category is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        if(id == 1){
            errorMessage = "You can't delete default category!";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        boolean result = new CategoryDao().deleteCategory(requestCategory);

        log.info("result --> " + result);
        if (!result) {
            request.getSession().setAttribute("getAlert", "deleteError");
        } else {
            request.getSession().setAttribute("getAlert", "success");
        }

        String currentPageString = request.getParameter("currentPage");
        log.trace("Request parameter: currentPage --> " + currentPageString);
        String recordsPerPageString = request.getParameter("recordsPerPage");
        log.trace("Request parameter: recordsPerPage --> " + recordsPerPageString);

        int currentPage = 1;
        int recordsPerPage = 5;

        if(currentPageString!= null && recordsPerPageString!=null){
            currentPage = Integer.parseInt(currentPageString);
            recordsPerPage = Integer.parseInt(recordsPerPageString);
        }

        forward = Path.COMMAND__ADMIN_LIST_CATEGORIES+"&recordsPerPage="+recordsPerPage+"&currentPage="+currentPage;


        log.debug("Command finished");
        log.trace("Set the attribute: forward --> " + forward);
        return forward;
    }

}
