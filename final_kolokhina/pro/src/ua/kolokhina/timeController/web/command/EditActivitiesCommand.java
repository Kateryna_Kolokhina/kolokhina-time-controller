package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;

import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.User;
import ua.kolokhina.timeController.db.entity.UserActivity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Time;

import java.util.*;

public class EditActivitiesCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(EditActivitiesCommand.class);


    public EditActivitiesCommand(boolean redirect) {
        super(redirect);
    }

    public EditActivitiesCommand() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");
        User user = (User)request.getSession().getAttribute("user");
        log.trace("Get the session attribute: user --> " + user);

        String access = user.getAccess();

        if(access.equals("disable")){
            String errorMessage = "You are not allowed to this action";
            request.setAttribute("errorMessage",errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return Path.PAGE__ERROR_PAGE;
        }

        if (request.getParameter("edit") != null) {
            return editActivities(request,response);

        } else if (request.getParameter("delete") != null) {
           return deleteActivities(request,response);
        }
        return Path.PAGE__ERROR_PAGE;
    }

    private String deleteActivities(HttpServletRequest request, HttpServletResponse response) {

        log.debug("Command deleting starts");

        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        User user = (User)request.getSession().getAttribute("user");
        log.trace("Get the session attribute: user --> " + user);

        String[] selectedId = request.getParameterValues("deleteId");
        log.trace("Request parameter: selectedId --> " + Arrays.toString(selectedId));

        if (selectedId == null || selectedId.length == 0){
            errorMessage = "Selected nothing to delete";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {
            log.info("User select to delete activities with id --> " + Arrays.toString(selectedId));

            List<Activity> deleteItems;

            deleteItems = new ActivitiesDao().findUserActivities(selectedId);
            log.trace("Found in DB: activitiesItemsList --> " + deleteItems.size());

            boolean result = new ActivitiesDao().deleteActivitiesForUser(user, deleteItems);
            log.info("result --> " + result);

            if(!result){
                errorMessage = "SQL exception: Deleting is failed ";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                request.getSession().setAttribute("getAlert", "deleteError");
                return forward;
            }else{
                request.getSession().setAttribute("getAlert", "success");
                forward = Path.COMMAND__USER_ACTIVITIES;
            }
        }
        log.debug("Command deleting finished");
        log.trace("Set the attribute: forward --> " + forward);
        return forward;
    }

    public String editActivities(HttpServletRequest request,HttpServletResponse response){

        log.debug("Command editing starts");
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        User user = (User)request.getSession().getAttribute("user");
        log.trace("Get the session attribute: user --> " + user);

        String[] timeString =request.getParameterValues("timeAmount");
        log.trace("Request parameter: timeAmount --> " + Arrays.toString(timeString));

        if (timeString == null || timeString.length == 0){
            errorMessage = "Selected nothing";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {
            log.info("User select activities with timeAmount --> " + Arrays.toString(timeString));

            List<UserActivity> userActivities = new ActivitiesDao().findUserActivities(user,2);
            log.trace("Found in DB: activitiesItemsList --> " + userActivities);

            ArrayList<UserActivity> requestToChange = new ArrayList<>();

            for (int i=0;i<userActivities.size();i++) {
                String dbTimeAmount = userActivities.get(i).getTimeAmount().toString();

                if(!dbTimeAmount.equals(timeString[i])
                        && !timeString[i].equals("00:00:00")){

                        UserActivity activity = userActivities.get(i);
                        activity.setTimeAmount(Time.valueOf(timeString[i]));
                        requestToChange.add(activity);
                }
            }
            log.trace("Set request value: requestToChange --> " + userActivities);

            boolean result = new ActivitiesDao().updateActivitiesForUser(user, requestToChange);
            log.info("result --> " + result);

            if(!result){
                errorMessage = "Edit exception: Editing is failed ";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return forward;
            }else{
                request.getSession().setAttribute("getAlert", "success");
                forward = Path.COMMAND__USER_ACTIVITIES;
            }
        }

        log.debug("Command editing finished");
        log.trace("Set the attribute: forward --> " + forward);
        return forward;
    }

}
