package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class EditActivityCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(EditActivityCommand.class);

    private String errorMessage;
    private String forward = Path.PAGE__ERROR_PAGE;

    public EditActivityCommand(){}
    public EditActivityCommand(boolean redirect) {
        super(redirect);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){

        String idString = request.getParameter("id");
        log.trace("Request parameter: id --> " + idString);

        long id;

        if(idString == null || idString.isEmpty()){
            errorMessage = "Id is null";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {
            try{
            id = Long.parseLong(request.getParameter("id"));
            }catch (NumberFormatException ex){
                errorMessage = "Id is non valid";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return forward;
            }
        }
        log.info("Get the activity attribute: id --> " + id);

        Activity activity = new ActivitiesDao().findActivity(id);
        log.trace("Found in DB: activity --> " + activity);

        if (activity == null) {
            errorMessage = "This activity is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        String name = request.getParameter("name");
        log.trace("Request parameter: name --> " + name);

        String description = request.getParameter("description");
        log.trace("Request parameter: description --> " + description);

        String category = request.getParameter("category");
        log.trace("Request parameter: category --> " + category);

        if(!validateData(name,description,category)){
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        Activity requestActivity = new Activity();
        requestActivity.setName(name);
        requestActivity.setDescription(description);

        try{
            requestActivity.setCategoryId(Long.valueOf(category));
        }catch (NumberFormatException ex){
            errorMessage = "Category is non valid";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        requestActivity.setId(id);

        Activity chechActivity = new ActivitiesDao().findActivity(name);
        if(requestActivity.equals(chechActivity) && !requestActivity.equals(activity)){
            errorMessage = "You create existed activity";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        Category requestCategory = new CategoryDao().findCategory(Long.valueOf(category));
        List<Category> categories = new CategoryDao().findCategories();
        log.trace("Found in DB: categories --> " + categories);
        if(!categories.contains(requestCategory)){
            errorMessage = "This category isn't exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        boolean result = new ActivitiesDao().editActivity(requestActivity);
        log.info("result --> " + result);

        if(!result){
            request.getSession().setAttribute("getAlert", "updateError");
        }else{
            request.setAttribute("name", null);
            request.setAttribute("description",null);
            request.getSession().setAttribute("getAlert", "success");
        }

        forward = Path.COMMAND__ADMIN_EDIT_ACTIVITY+"&id="+id;
        log.debug("Command finished");
        log.trace("Set the attribute: forward --> " + forward);
        return forward;
    }
    public boolean validateData(String name,String description,String category){
        if (name == null || description == null || category == null ||
                name.isEmpty() || description.isEmpty() || category.isEmpty()) {
            errorMessage = "Input data cannot be empty";
            log.error("errorMessage --> " + errorMessage);
            return false;
        }
        return true;
    }
}
