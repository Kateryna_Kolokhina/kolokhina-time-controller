package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.IllegalFormatException;
import java.util.List;

public class EditActivityPage extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(EditActivityPage.class);

    public EditActivityPage(boolean redirect) {
        super(redirect);
    }

    public EditActivityPage() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");
        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        String idString = request.getParameter("id");
        log.trace("Request parameter: id --> " + idString);

        long id;
        if(idString == null || idString.isEmpty()){
            errorMessage = "Id is null";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {
            try {
                id = Long.parseLong(request.getParameter("id"));
            }catch (NumberFormatException ex){
                errorMessage = "Id is non valid";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return forward;
            }
        }
        log.info("Get the activity attribute: id --> " + id);

        Activity activity = new ActivitiesDao().findActivity(id);
        log.trace("Found in DB: activity --> " + activity);

        if (activity == null) {
            errorMessage = "This activity is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        request.setAttribute("activity", activity);
        log.trace("Set the request attribute: activity --> " + activity);

        List<Category> listCategories = new CategoryDao().findCategories();
        log.trace("Found in DB: listCategories --> " + listCategories);

        request.setAttribute("listCategories", listCategories);
        log.trace("Set the request attribute: listCategories --> " + listCategories);

        log.debug("Command finished");
        return Path.PAGE__ADMIN_EDIT_ACTIVITY;
    }
}
