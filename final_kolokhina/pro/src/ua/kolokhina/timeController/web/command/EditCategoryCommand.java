package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditCategoryCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(EditCategoryCommand.class);

    private String errorMessage;
    private String forward = Path.PAGE__ERROR_PAGE;

    public EditCategoryCommand(){}
    public EditCategoryCommand(boolean redirect) {
        super(redirect);
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");


        String idString = request.getParameter("id");
        log.trace("Request parameter: id --> " + idString);

        long id;
        if(idString == null || idString.isEmpty()){
            errorMessage = "Id is null";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {
            try {
                id = Long.parseLong(request.getParameter("id"));
            }catch (NumberFormatException ex){
                errorMessage = "Id is non valid";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return forward;
            }
        }
        log.info("Get the category attribute: id --> " + id);

        Category category = new CategoryDao().findCategory(id);
        log.trace("Found in DB: category --> " + category);

        if (category == null) {
            errorMessage = "This category is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        if(id == 1){
            errorMessage = "You can't edit default category!";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        String name = request.getParameter("name");
        log.trace("Request parameter: name --> " + name);

        String description = request.getParameter("description");
        log.trace("Request parameter: description --> " + description);

        if (!validateData(name,description)) {
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        Category requestCategory = new Category();
        requestCategory.setName(name);
        requestCategory.setDescription(description);
        requestCategory.setId(id);

        Category chechCategory = new CategoryDao().findCategory(name);
        if(requestCategory.equals(chechCategory) && !requestCategory.equals(category)){
            errorMessage = "You create existed category";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        boolean result = new CategoryDao().editCategory(requestCategory);
        log.info("result --> " + result);

        if(!result){
            request.getSession().setAttribute("getAlert", "updateError");
        }else{
            request.setAttribute("name", null);
            request.setAttribute("description",null);
            request.getSession().setAttribute("getAlert", "success");
        }

        forward = Path.COMMAND__ADMIN_EDIT_CATEGORY+"&id="+id;
        log.debug("Command finished");
        log.trace("Set the  attribute: forward --> " + forward);
        return forward;
    }

    public boolean validateData(String name,String description){
        if (name == null || description == null ||
                name.isEmpty() || description.isEmpty()) {
            errorMessage = "Input data cannot be empty";
            log.error("errorMessage --> " + errorMessage);
            return false;
        }
        return true;
    }
}
