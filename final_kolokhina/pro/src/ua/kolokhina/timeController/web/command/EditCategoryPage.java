package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EditCategoryPage extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(EditCategoryPage.class);


    public EditCategoryPage(boolean redirect) {
        super(redirect);
    }

    public EditCategoryPage() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");

        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        String idString = request.getParameter("id");
        log.trace("Request parameter: id --> " + idString);

        long id;

        if(idString == null || idString.isEmpty()){
            errorMessage = "Id is null";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }else {

            try{
            id = Long.parseLong(request.getParameter("id"));
            }catch (NumberFormatException ex){
                errorMessage = "Id is non valid";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return forward;
            }
        }

        log.info("Get the category attribute: id --> " + id);

        if(id == 1){
            errorMessage = "You can't edit default category!";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        Category category = new CategoryDao().findCategory(id);
        log.trace("Found in DB: category --> " + category);

        if (category == null) {
            errorMessage = "This category is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        request.setAttribute("category", category);
        log.trace("Set the request attribute: category --> " + category);
        log.debug("Command finished");
        return Path.PAGE__ADMIN_EDIT_CATEGORY;
    }
}
