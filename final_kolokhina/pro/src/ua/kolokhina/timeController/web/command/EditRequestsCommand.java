package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.entity.UserRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class EditRequestsCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(EditRequestsCommand.class);


    public EditRequestsCommand(boolean redirect) {
        super(redirect);
    }

    public EditRequestsCommand() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){

        if (request.getParameter("add") != null) {
            return addActivities(request,response);

        }else if (request.getParameter("delete") != null) {
            return deleteActivities(request,response);
        }
        return Path.PAGE__ERROR_PAGE;
    }
    public String addActivities(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command add starts");

        List<UserRequest> requestListAdd = new ActivitiesDao().findUsersRequests(1);
        log.trace("Found in DB: requestListAdd --> " + requestListAdd);


        ArrayList<UserRequest> confirm = new ArrayList<>();
        ArrayList<UserRequest> cancel = new ArrayList<>();
        String action;
        for ( int i =0;i<requestListAdd.size();i++) {
            action = request.getParameter(String.valueOf(i));
            if(action == null)
                continue;
            if(action.equals("confirm")){
                confirm.add(requestListAdd.get(i));
            }else if(action.equals("cancel")){
                cancel.add(requestListAdd.get(i));
            }
        }

        boolean resultAdd = new ActivitiesDao().confirmRequests(confirm,"add");
        log.info("result add --> " + resultAdd);
        boolean resultCancel = new ActivitiesDao().cancelRequests(cancel,"add");
        log.info("result cancel --> " + resultCancel);

        if(!resultAdd){
            request.getSession().setAttribute("getAlert", "confirmError");
        }else if(!resultCancel) {
            request.getSession().setAttribute("getAlert", "cancelError");
        }else{
            request.getSession().setAttribute("getAlert", "success");
        }
        log.debug("Command add finished");
        return Path.COMMAND__ADMIN_LIST_REQUESTS;
    }

    public String deleteActivities(HttpServletRequest request, HttpServletResponse response){

        log.debug("Command delete starts");


        List<UserRequest> requestListAdd = new ActivitiesDao().findUsersRequests(4);
        log.trace("Found in DB: requestListAdd --> " + requestListAdd);

        ArrayList<UserRequest> confirm = new ArrayList<>();
        ArrayList<UserRequest> cancel = new ArrayList<>();
        String action;
        for ( int i =0;i<requestListAdd.size();i++) {
            action = request.getParameter(String.valueOf(i));
            if(action == null)
                continue;
            if(action.equals("confirm")){
                confirm.add(requestListAdd.get(i));
            }else if(action.equals("cancel")){
                cancel.add(requestListAdd.get(i));
            }
        }

        boolean resultAdd = new ActivitiesDao().confirmRequests(confirm,"delete");
        log.info("result add --> " + resultAdd);
        boolean resultCancel = new ActivitiesDao().cancelRequests(cancel,"delete");
        log.info("result cancel --> " + resultCancel);
        if(!resultAdd){
            request.getSession().setAttribute("getAlert", "confirmError");
        }else if(!resultCancel) {
            request.getSession().setAttribute("getAlert", "cancelError");
        }else{
            request.getSession().setAttribute("getAlert", "success");
        }
        log.debug("Command delete finished");
        return Path.COMMAND__ADMIN_LIST_REQUESTS;
    }

}
