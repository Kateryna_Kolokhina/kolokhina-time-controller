package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListAccountsCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger log = Logger.getLogger(ListAccountsCommand.class);


	public ListAccountsCommand(boolean redirect) {
		super(redirect);
	}

    public ListAccountsCommand() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
		log.debug("Command starts");

		int currentPage;
		int recordsPerPage;

		String currentPageString = request.getParameter("currentPage");
		log.trace("Request parameter: currentPage --> " + currentPageString);

		String recordsPerPageString = request.getParameter("recordsPerPage");
		log.trace("Request parameter: recordsPerPage --> " + recordsPerPageString);

		if(currentPageString == null || currentPageString.isEmpty() ||
		recordsPerPageString == null || recordsPerPageString.isEmpty()){
			currentPage = 1;
			recordsPerPage = 5;
		}else {
			currentPage = Integer.parseInt(currentPageString);
			recordsPerPage = Integer.parseInt(recordsPerPageString);
		}

		List<User> accountsList = new UserDao().findUsers(currentPage,
				recordsPerPage);
		log.trace("Found in DB: accountsList --> " + accountsList);

		request.setAttribute("accountsList", accountsList);
		log.trace("Set the request attribute: accountsList --> " + accountsList);

		int rows = new UserDao().getNumberOfRows();
		log.trace("Set the request attribute: rows --> " + rows);

		int nOfPages = rows / recordsPerPage;

		if (rows % recordsPerPage > 0) {
			nOfPages++;
		}

		request.setAttribute("noOfPages", nOfPages);
		log.trace("Set the request attribute: noOfPages --> " + nOfPages);

		request.setAttribute("currentPage", currentPage);
		log.trace("Set the request attribute: currentPage --> " + currentPage);

		request.setAttribute("recordsPerPage", recordsPerPage);
		log.trace("Set the request attribute: recordsPerPage --> " + recordsPerPage);

		log.debug("Command finished");
		log.trace("Set the attribute: forward --> " + Path.PAGE__ADMIN_LIST_ACCOUNTS);

		return Path.PAGE__ADMIN_LIST_ACCOUNTS;
	}

}