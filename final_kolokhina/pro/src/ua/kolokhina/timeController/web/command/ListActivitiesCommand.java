package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Activity;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;


public class ListActivitiesCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger log = Logger.getLogger(ListActivitiesCommand.class);

	public ListActivitiesCommand(boolean redirect) {
		super(redirect);
	}

    public ListActivitiesCommand() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
		log.debug("Command starts");

		List<Activity> listActivitiesItems = new ActivitiesDao().findActivities("enable");
		log.trace("Found in DB: activitiesItemsList --> " + listActivitiesItems);

		List<Category> listCategories = new CategoryDao().findCategories();
		log.trace("Found in DB: listCategories --> " + listCategories);

		request.setAttribute("listActivitiesItems", listActivitiesItems);
		log.trace("Set the request attribute: listActivitiesItems --> " + listActivitiesItems);

		request.setAttribute("listCategories", listCategories);
		log.trace("Set the request attribute: listCategories --> " + listCategories);

		log.debug("Command finished");
		return Path.PAGE__LIST_ACTIVITIES;
	}

}