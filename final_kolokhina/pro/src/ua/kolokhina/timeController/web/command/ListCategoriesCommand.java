package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.Category;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;


public class ListCategoriesCommand extends Command {

    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(ListCategoriesCommand.class);


    public ListCategoriesCommand(boolean redirect) {
        super(redirect);
    }

    public ListCategoriesCommand() {
    }
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Command starts");

        int currentPage;
        int recordsPerPage;

        String currentPageString = request.getParameter("currentPage");
        log.trace("Request parameter: currentPage --> " + currentPageString);

        String recordsPerPageString = request.getParameter("recordsPerPage");
        log.trace("Request parameter: recordsPerPage --> " + recordsPerPageString);

        if(currentPageString == null || currentPageString.isEmpty() ||
                recordsPerPageString == null || recordsPerPageString.isEmpty()){
            currentPage = 1;
            recordsPerPage = 5;
        }else {
            currentPage = Integer.parseInt(currentPageString);
            recordsPerPage = Integer.parseInt(recordsPerPageString);
        }

        log.info("currentPage --> " + currentPage);
        log.info("recordsPerPage --> " + recordsPerPage);

        String sort_type = request.getParameter("sort");
        log.trace("Request parameter: sort_type --> " + sort_type);

        if(sort_type == null || sort_type.isEmpty())
            sort_type = "id";

        List<Category> categoryList = new CategoryDao().findCategories(currentPage,
                recordsPerPage);

        log.trace("Found in DB: categoryList --> " + categoryList);

        request.setAttribute("categoryList", categoryList);
        log.trace("Set the request attribute: categoryList --> " + categoryList);

        int rows = new CategoryDao().getNumberOfRowsCategory() ;

        int nOfPages = rows / recordsPerPage;
        if (rows % recordsPerPage > 0) {
            nOfPages++;
        }

        request.setAttribute("noOfPages", nOfPages);
        log.trace("Set the request attribute: noOfPages --> " + nOfPages);

        request.setAttribute("currentPage", currentPage);
        log.trace("Set the request attribute: currentPage --> " + currentPage);

        request.setAttribute("recordsPerPage", recordsPerPage);
        log.trace("Set the request attribute: recordsPerPage --> " + recordsPerPage);

        log.debug("Command finished");
        log.trace("Set the attribute: forward --> " + Path.PAGE__ADMIN_LIST_CATEGORIES);
        return Path.PAGE__ADMIN_LIST_CATEGORIES;
    }
}
