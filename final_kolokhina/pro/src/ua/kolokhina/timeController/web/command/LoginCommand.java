package ua.kolokhina.timeController.web.command;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.Role;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

public class LoginCommand extends Command {

	private static final long serialVersionUID = -3071536593627692473L;
	
	private static final Logger log = Logger.getLogger(LoginCommand.class);

	private String errorMessage = null;
	String forward = Path.PAGE__ERROR_PAGE;


	public LoginCommand(boolean redirect) {
		super(redirect);
	}

    public LoginCommand() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		log.debug("Command starts");

		HttpSession session = request.getSession();


		HttpSession checkSession = request.getSession(false);
		Role checkUserRole = (Role)checkSession.getAttribute("userRole");

		if (checkUserRole != null){
			errorMessage = "User is already signed in";
			request.setAttribute("errorMessage", errorMessage);
			log.error("errorMessage --> " + errorMessage);
			return forward;
		}

		// obtain login and password from the request
		String login = request.getParameter("login");
		log.trace("Request parameter: login --> " + login);
		
		String password = request.getParameter("password");
		log.trace("Request parameter: password --> " + password);


		if(!validateData(login,password)){
			request.setAttribute("errorMessage", errorMessage);
			return forward;
		}

		User user = new UserDao().findUserByLogin(login);

		log.trace("Found in DB: user --> " + user);
			
		if (user == null) {
			errorMessage = "Cannot find user with such login";
			request.setAttribute("errorMessage", errorMessage);
			log.error("errorMessage --> " + errorMessage);
			return forward;
		}else if(!BCrypt.verifyer().verify(password.toCharArray(), user.getPassword()).verified){
			errorMessage = "Wrong password!";
			request.setAttribute("errorMessage", errorMessage);
			log.error("errorMessage --> " + errorMessage);
			return forward;
		} else{
			Role userRole = Role.getRole(user);
			log.trace("userRole --> " + userRole);
				
			if (userRole == Role.ADMIN) {
				forward = Path.COMMAND__ADMIN_LIST_ACCOUNTS;
			}

			if (userRole == Role.CLIENT) {
				forward = Path.COMMAND__USER_ACTIVITIES;
			}

			session.setAttribute("user", user);
			log.trace("Set the session attribute: user --> " + user);
				
			session.setAttribute("userRole", userRole);				
			log.trace("Set the session attribute: userRole --> " + userRole);
				
			log.info("User " + user + " logged as " + userRole.toString().toLowerCase());

			String userLocaleName = user.getLocaleName();
			log.trace("userLocalName --> " + userLocaleName);
			
			if (userLocaleName != null && !userLocaleName.isEmpty()) {
				Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
				
				session.setAttribute("defaultLocale", userLocaleName);
				log.trace("Set the session attribute: defaultLocaleName --> " + userLocaleName);
				
				log.info("Locale for user: defaultLocale --> " + userLocaleName);
			}
		}

		log.debug("Command finished");

		log.trace("Set the  attribute: forward --> " + forward);
		return forward;
	}

	public boolean validateData(String login,String password){
		if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
			errorMessage = "Login/password cannot be empty";
			log.error("errorMessage --> " + errorMessage);
			return false;
		}
		return true;
	}
}