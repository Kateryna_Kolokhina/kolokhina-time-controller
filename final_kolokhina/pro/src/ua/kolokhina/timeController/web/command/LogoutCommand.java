package ua.kolokhina.timeController.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kolokhina.timeController.Path;


public class LogoutCommand extends Command {

	private static final long serialVersionUID = -2785976616686657267L;

	private static final Logger log = Logger.getLogger(LogoutCommand.class);


	public LogoutCommand(boolean redirect) {
		super(redirect);
	}

	public LogoutCommand() {
    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
		log.debug("Command starts");

		String errorMessage;
		String forward = Path.PAGE__ERROR_PAGE;
		HttpSession session = request.getSession(false);

		if (session != null)
			session.invalidate();
		else{
			errorMessage = "No started sessions found";
			request.setAttribute("errorMessage", errorMessage);
			log.error("errorMessage --> " + errorMessage);
			return forward;
		}
		log.debug("Command finished");
		return Path.PAGE__LOGIN;
	}

}