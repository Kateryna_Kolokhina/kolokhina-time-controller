package ua.kolokhina.timeController.web.command;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.apache.log4j.Logger;

import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.Role;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Login command.
 *
 * @author D.Kolesnikov
 *
 */
public class RegistrationCommand extends Command {

    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(LoginCommand.class);

    private static final String LOGIN_PATTERN =
            "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$";
    private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z]).{8,32}$";
    private static final String EMAIL_PATTERN = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";

    private String errorMessage = null;
    private String forward = Path.PAGE__ERROR_PAGE;

    public RegistrationCommand() {
    }
    public RegistrationCommand(boolean redirect) {
        super(redirect);
    }

    private static boolean isValid(final String value,String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){

        log.debug("Command starts");

        HttpSession session = request.getSession();

        HttpSession checkSession = request.getSession(false);
        Role checkUserRole = (Role)checkSession.getAttribute("userRole");
        log.trace("Session attribute: userRole --> " + checkUserRole);

        if (checkUserRole != null){
            errorMessage = "User is already signed in";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        String login = request.getParameter("login");
        log.trace("Request parameter: login --> " + login);

        String password = request.getParameter("password");
        log.trace("Request parameter: password --> " + password);

        String email = request.getParameter("email");
        log.trace("Request parameter: email --> " + email);

        String password_confirm = request.getParameter("password_confirmation");
        log.trace("Request parameter: password_confirm --> " + password_confirm);

        if(!validateData(login, password,email,password_confirm)){
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        User user = new UserDao().findUserByLogin(login);
        log.trace("Found in DB: user --> " + user);

        if (user != null && !user.getEmail().equals(email)) {
            errorMessage = "User with this login is already registered";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        } else {
            String bcryptHashString = BCrypt.withDefaults().hashToString(12, password.toCharArray());
            User insertUser = User.createUser(login,email,bcryptHashString,1);

            boolean result = new UserDao().addUser(insertUser);
            if(!result){
                errorMessage = "SQL Insert error";
                request.setAttribute("errorMessage", errorMessage);
                log.error("errorMessage --> " + errorMessage);
                return forward;
            }else{
                Role userRole = Role.getRole(insertUser);

                forward = Path.COMMAND__USER_ACTIVITIES;

                session.setAttribute("user", insertUser);
                log.trace("Set the session attribute: user --> " + insertUser);

                session.setAttribute("userRole", userRole);
                log.trace("Set the session attribute: userRole --> " + userRole);

                log.info("User " + insertUser + " logged as " + userRole.toString().toLowerCase());

                // work with i18n
                String userLocaleName = insertUser.getLocaleName();
                log.trace("userLocalName --> " + userLocaleName);

                if (userLocaleName != null && !userLocaleName.isEmpty()) {
                    Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);

                    session.setAttribute("defaultLocale", userLocaleName);
                    log.trace("Set the session attribute: defaultLocaleName --> " + userLocaleName);

                    log.info("Locale for user: defaultLocale --> " + userLocaleName);
                }
            }
        }

        log.debug("Command finished");
        log.trace("Set the  attribute: forward --> " + forward);
        return forward;
    }
    public boolean validateData(String login,String password,String email,String password_confirm){
        if (login == null || password == null || email == null || password_confirm == null
                || login.isEmpty() || password.isEmpty() || email.isEmpty() || password_confirm.isEmpty()) {
            errorMessage = "Input data cannot be empty";
            log.error("errorMessage --> " + errorMessage);
            return false;
        }else if(!isValid(login,LOGIN_PATTERN) || !isValid(password,PASSWORD_PATTERN) || !isValid(email,EMAIL_PATTERN)) {
            errorMessage = "Input data is invalid";
            log.error("errorMessage --> " + errorMessage);
            return false;
        }
        return true;
    }

}