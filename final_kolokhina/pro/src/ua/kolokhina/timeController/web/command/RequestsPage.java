package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;

import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.entity.UserRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class RequestsPage extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger log = Logger.getLogger(RequestsPage.class);


	public RequestsPage(boolean redirect) {
		super(redirect);
	}

    public RequestsPage() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
		log.debug("Command starts");

		List<UserRequest> requestListAdd = new ActivitiesDao().findUsersRequests(1);
		log.trace("Found in DB: requestListAdd --> " + requestListAdd);

		request.setAttribute("requestListAdd", requestListAdd);
		log.trace("Set the request attribute: requestListAdd --> " + requestListAdd);

		List<UserRequest> requestListDelete = new ActivitiesDao().findUsersRequests(4);
		log.trace("Found in DB: requestListDelete --> " + requestListDelete);

		request.setAttribute("requestListDelete", requestListDelete);
		log.trace("Set the request attribute: requestListDelete --> " + requestListDelete);

		log.debug("Command finished");
		return Path.PAGE__ADMIN_LIST_REQUESTS;
	}

}