package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UpdateAccountCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(UpdateAccountCommand.class);

    public UpdateAccountCommand(boolean redirect) {
        super(redirect);
    }

    public UpdateAccountCommand() {
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Command edit starts");

        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        String id = request.getParameter("id");
        log.trace("Request parameter: id --> " + id);

        String access = request.getParameter("access");
        log.trace("Request parameter: access --> " + access);


        if (id == null || id.isEmpty() || access == null || access.isEmpty() ||
                ( !access.equals("enable") && !access.equals("disable") ) ){
            errorMessage = "Input data is empty or not correct";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        User user = new UserDao().findUser(Long.valueOf(id));
        log.trace("Found in DB: user --> " + user);

        if(user==null){
            errorMessage = "User with id "+id+" is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        boolean result = new UserDao().setAccessUser(user,access);
        log.info("result --> " + result);

        if(!result){
            request.getSession().setAttribute("getAlert", "updateError");
        }
        request.getSession().setAttribute("getAlert", "success");


        String currentPageString = request.getParameter("currentPage");
        log.trace("Request parameter: currentPage --> " + currentPageString);

        String recordsPerPageString = request.getParameter("recordsPerPage");
        log.trace("Request parameter: recordsPerPage --> " + recordsPerPageString);


        int currentPage = 1;
        int recordsPerPage = 5;

        if(currentPageString!= null && recordsPerPageString!=null){
            currentPage = Integer.parseInt(currentPageString);
            recordsPerPage = Integer.parseInt(recordsPerPageString);
        }

        request.setAttribute("currentPage", currentPage);
        log.trace("Set the request attribute: currentPage --> " + currentPage);

        request.setAttribute("recordsPerPage", recordsPerPage);
        log.trace("Set the request attribute: recordsPerPage --> " + recordsPerPage);

        forward = Path.COMMAND__ADMIN_LIST_ACCOUNTS+"&recordsPerPage="+recordsPerPage+"&currentPage="+currentPage;

        log.debug("Command finished");
        log.trace("Set the attribute: forward --> " +forward);
        return forward;
    }
}
