package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;


public class UpdateSettingsCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;

	private static final Logger log = Logger.getLogger(UpdateSettingsCommand.class);


	public UpdateSettingsCommand(boolean redirect) {
		super(redirect);
	}

    public UpdateSettingsCommand() {

    }

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response){
		
		log.debug("Command starts");
		
		// UPDATE USER ////////////////////////////////////////////////////////
		
		User user = (User)request.getSession().getAttribute("user");
		log.trace("Get session attribute: user --> "+ user);
		boolean updateUser = false;

		String localeToSet = request.getParameter("localeToSet");
		log.trace("Request parameter: localeToSet --> " + localeToSet);

		if (localeToSet != null && !localeToSet.isEmpty()) {
			HttpSession session = request.getSession();
			Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", localeToSet);			
			session.setAttribute("defaultLocale", localeToSet);
			user.setLocaleName(localeToSet);
			updateUser = true;
		}
		
		if (updateUser)
			new UserDao().updateUser(user);

		
		log.debug("Command finished");
		return Path.COMMAND__VIEW_SETTINGS;
	}

}