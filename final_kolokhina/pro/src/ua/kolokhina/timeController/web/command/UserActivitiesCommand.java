package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.entity.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

public class UserActivitiesCommand extends Command {

    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(ListActivitiesCommand.class);


    public UserActivitiesCommand(boolean redirect) {
        super(redirect);
    }

    public UserActivitiesCommand() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");

        User user = (User)request.getSession().getAttribute("user");
        log.trace("Get the session attribute: user --> " + user);

        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;
        if(user == null ){
            errorMessage = "No active user";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        List<UserActivity> userActivities = new ActivitiesDao().findUserActivities(user,2);
        log.trace("Found in DB: activitiesItemsList --> " + userActivities);

        List<Category> listCategories = new CategoryDao().findCategories();
        request.setAttribute("listCategories", listCategories);
        log.trace("Set the request attribute: listCategories --> " + listCategories);

        List<UserRequest> requests = new ActivitiesDao().findUserRequests(user);
        log.trace("Found in DB: requests --> " + requests);
        request.setAttribute("requests", requests);
        log.trace("Set the request attribute: requests --> " + requests);

        request.setAttribute("userActivities", userActivities);
        log.trace("Set the request attribute: userActivities --> " + userActivities);


        List<Status> statuses = new ActivitiesDao().findStatusesInformation();
        log.trace("Found in DB: statuses --> " + statuses);
        request.setAttribute("statuses", statuses);
        log.trace("Set the request attribute: statuses --> " + statuses);

        log.debug("Command finished");

        log.trace("Set the attribute: forward --> " + Path.PAGE__USER_ACTIVITIES);
        return Path.PAGE__USER_ACTIVITIES;
    }

}
