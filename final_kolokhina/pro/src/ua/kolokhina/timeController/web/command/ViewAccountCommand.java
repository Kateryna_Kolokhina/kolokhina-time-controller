package ua.kolokhina.timeController.web.command;

import org.apache.log4j.Logger;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.ActivitiesDao;
import ua.kolokhina.timeController.db.CategoryDao;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.Category;
import ua.kolokhina.timeController.db.entity.User;
import ua.kolokhina.timeController.db.entity.UserActivity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ViewAccountCommand extends Command {
    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(ViewAccountCommand.class);


    public ViewAccountCommand(boolean redirect) {
        super(redirect);
    }

    public ViewAccountCommand() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command starts");

        String errorMessage;
        String forward = Path.PAGE__ERROR_PAGE;

        String id = request.getParameter("id");
        log.trace("Request parameter: id --> " + id);

        if (id == null || id.isEmpty() ){
            errorMessage = "Input data is empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }
        User user = new UserDao().findUser(Long.valueOf(id));
        log.trace("Found in DB: user --> " + user);

        if(user==null){
            errorMessage = "User with id "+id+"is not exist";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return forward;
        }

        request.setAttribute("infoUser", user);
        log.trace("Set the request attribute: infoUser --> " + user);

        List<UserActivity> userActivities = new ActivitiesDao().findUserActivities(user,2);
        log.trace("Found in DB: activitiesItemsList --> " + userActivities);


        List<Category> listCategories = new CategoryDao().findCategories();
        request.setAttribute("listCategories", listCategories);
        log.trace("Set the request attribute: listCategories --> " + listCategories);

        request.setAttribute("userActivities", userActivities);
        log.trace("Set the request attribute: userActivities --> " + userActivities);

        log.debug("Command finished");
        log.trace("Set the attribute: forward --> " + Path.PAGE__ADMIN_USER_INFO);
        return Path.PAGE__ADMIN_USER_INFO;
    }
}
