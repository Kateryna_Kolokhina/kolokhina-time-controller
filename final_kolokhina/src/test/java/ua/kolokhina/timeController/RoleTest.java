package ua.kolokhina.timeController;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ua.kolokhina.timeController.db.entity.User;

import static org.junit.jupiter.api.Assertions.*;

class RoleTest {

    private static User client,admin;
    @BeforeAll
    static void beforeAll() {
        User client = new User();
        client.setLogin("user");
        client.setRole(1);
        User admin = new User();
        admin.setLogin("admin");
        admin.setRole(2);
        setClient(client);
        setAdmin(admin);
    }

    public static void setClient(User client) {
        RoleTest.client = client;
    }

    public static void setAdmin(User admin) {
        RoleTest.admin = admin;
    }

    @Test
    void getRoleClient() {
        Role userRole = Role.getRole(client);

        assertEquals(Role.CLIENT, userRole);
    }

    @Test
    void getRoleAdmin() {
        Role userRole = Role.getRole(admin);
        assertEquals(Role.ADMIN, userRole);
    }

    @Test
    void getName() {
        assertEquals(Role.ADMIN.getName(),"admin");
    }
}