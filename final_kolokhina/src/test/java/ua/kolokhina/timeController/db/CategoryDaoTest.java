package ua.kolokhina.timeController.db;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testng.annotations.BeforeTest;
import ua.kolokhina.timeController.db.entity.Category;

import java.io.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.testng.Assert.*;

class CategoryDaoTest {

    private static DBManager dbManager;

    @BeforeTest
    static void setUp(){
        BasicConfigurator.configure();
        dbManager = DBManager.getInstance(true);
        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @BeforeAll
    static void beforeAll() {
        BasicConfigurator.configure();
        dbManager = DBManager.getInstance(true);
        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getNumberOfRowsCategory() {
        int allCategories = new CategoryDao(true).findCategories().size();
        int rows = new CategoryDao(true).getNumberOfRowsCategory();
        assertEquals(allCategories, rows);
    }


    @Test
    void findCategoriesTrue() {
        int numb = new CategoryDao(true).findCategories(1,7).size();

        assertEquals(5, numb);
    }
    @Test
    void findCategoriesFalse() {
        int numb = new CategoryDao(true).findCategories(3,7).size();
        assertEquals(0, numb);
    }
    @Test
    void testFindCategories() {
        int allCategories = new CategoryDao(true).findCategories().size();
        int rows = new CategoryDao(true).getNumberOfRowsCategory();
        assertEquals(allCategories, rows);
    }

    @Test
    void findCategory() {
        String name = new CategoryDao(true).findCategory((long)2).getName();
        assertEquals(name,"needs");
    }

    @Test
    void findCategoryFalse() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            String name = new CategoryDao(true).findCategory((long)7).getName();
        });
    }

    @Test
    void addCategory() {
        Category cat = new Category();
        cat.setName("test");
        cat.setDescription("description");
        boolean res = new CategoryDao(true).addCategory(cat);
        assertTrue(res);
    }

    @Test
    void editCategory() {
        Category cat = new Category();
        cat.setId((long) 2);
        cat.setName("test");
        cat.setDescription("description");
        boolean res = new CategoryDao(true).editCategory(cat);
        assertTrue(res);
    }

    @Test
    void deleteCategory() {
        Category cat = new Category();
        cat.setId((long) 2);
        cat.setName("test");
        cat.setDescription("description");
        boolean res = new CategoryDao(true).deleteCategory(cat);
        assertTrue(res);
    }

    @Test
    void deleteCategoryNotExist() {
        Category cat = new Category();
        cat.setId((long) 7);
        cat.setName("test");
        cat.setDescription("description");
        boolean res = new CategoryDao(true).deleteCategory(cat);
        assertFalse(res);
    }

}