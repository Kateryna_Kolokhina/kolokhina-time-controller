package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class ActivitiesAdminCommandTest {

    private static ActivitiesAdminCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new ActivitiesAdminCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateAccessTrue() {
        String access = "enable";
        test = new ActivitiesAdminCommand();
        assertTrue(test.validateAccess(access));
    }

    @Test
    void validateAccessFalse() {
        String access = "";
        test = new ActivitiesAdminCommand();
        assertFalse(test.validateAccess(access));
    }

    @Test
    void executeWrongId() {
        request.addParameter("update","update");
        request.addParameter("activityId","");
        test = new ActivitiesAdminCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeNotExistId() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter("update","update");
        request.addParameter("activityId","100");

        test = new ActivitiesAdminCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeId() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter("update","update");
        request.addParameter("activityId","3");
        test = new ActivitiesAdminCommand();

        assertEquals(Path.PAGE__ADMIN_LIST_ACTIVITIES, test.execute(request,response));
    }
    @Test
    void executeSortNotExist() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        request.addParameter("update","update");
        request.addParameter("activityId","3");
        request.addParameter("category","7");
        test = new ActivitiesAdminCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
}