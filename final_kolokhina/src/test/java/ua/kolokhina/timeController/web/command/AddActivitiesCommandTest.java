package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddActivitiesCommandTest {


    private static AddActivitiesCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new AddActivitiesCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void executeNoParameter() {

        User user = new UserDao(true).findUserByLogin("UserKate");
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("user", user);
        session.setAttribute("userRole", user.getRole());
        request.setSession(session);
        test = new AddActivitiesCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeAddNoSelected() {

        User user = new UserDao(true).findUserByLogin("UserKate");
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("user", user);
        session.setAttribute("userRole", user.getRole());
        request.setSession(session);
        request.setAttribute("add","add");
        test = new AddActivitiesCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeAdd() {

        User user = new UserDao(true).findUserByLogin("UserKate");
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("user", user);
        request.setSession(session);

        Integer[] selected = {1,3,5};
        request.setAttribute("itemId",selected);
        request.setAttribute("add","add");
        test = new AddActivitiesCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
}