package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class AddActivityCommandTest {


    private static AddActivityCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new AddActivityCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateDataFalse() {
        String name = "test";
        String description = null;
        String category = "";
        AddActivityCommand command = new AddActivityCommand();
        assertFalse(command.validateData(name,description,category));
    }
    @Test
    void validateDataTrue() {
        String name = "test";
        String description = "test";
        String category = "test";
        AddActivityCommand command = new AddActivityCommand();
        assertTrue(command.validateData(name,description,category));
    }

    @Test
    void executeInvalidData() {
        String name = "test";
        String description = "";
        String category = "";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.setParameter("category",category);

        test = new AddActivityCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeInvalidCategory() {
        String name = "test";
        String description = "secondDescription";
        String category = "6";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.setParameter("category",category);

        test = new AddActivityCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeTrue() {
        String name = "test";
        String description = "secondDescription";
        String category = "2";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.setParameter("category",category);

        test = new AddActivityCommand();

        assertEquals(Path.PAGE__ADMIN_ADD_ACTIVITY,test.execute(request,response));
    }


}