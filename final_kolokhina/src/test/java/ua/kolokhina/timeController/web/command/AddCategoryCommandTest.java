package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class AddCategoryCommandTest {

    private static AddCategoryCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new AddCategoryCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateDataFalse() {
        String name = "test";
        String description = null;
        test= new AddCategoryCommand();
        assertFalse(test.validateData(name,description));
    }
    @Test
    void validateDataTrue() {
        String name = "test";
        String description = "test";
        test= new AddCategoryCommand();
        assertTrue(test.validateData(name,description));
    }

    @Test
    void executeInvalidData() {
        String name = "test";
        String description = "";
        request.setParameter("name",name);
        request.setParameter("description",description);
        test = new AddCategoryCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeExistCategory() {
        String name = "needs";
        String description = "testDescr";
        request.setParameter("name",name);
        request.setParameter("description",description);

        test = new AddCategoryCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeTrue() {
        String name = "test5";
        String description = "secondDescription";
        request.setParameter("name",name);
        request.setParameter("description",description);
        test = new AddCategoryCommand();

        assertEquals(Path.PAGE__ADMIN_ADD_CATEGORY,test.execute(request,response));
    }
}