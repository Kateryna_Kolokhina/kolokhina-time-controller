package ua.kolokhina.timeController.web.command;

import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import static org.junit.jupiter.api.Assertions.*;

class AddCategoryPageTest {
    private static AddCategoryPage test;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        BasicConfigurator.configure();
        test = new AddCategoryPage();
    }
    @Test
    void execute() {
        assertEquals(Path.PAGE__ADMIN_ADD_CATEGORY,test.execute(request,response));
    }
}