package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class EditActivityCommandTest {


    private static EditActivityCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new EditActivityCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateDataFalse() {
        String name = "test";
        String description = null;
        String category = "";
        EditActivityCommand command = new EditActivityCommand();
        assertFalse(command.validateData(name,description,category));
    }
    @Test
    void validateDataTrue() {
        String name = "test";
        String description = "test";
        String category = "test";
        EditActivityCommand command = new EditActivityCommand();
        assertTrue(command.validateData(name,description,category));
    }

    @Test
    void executeInvalidCategory() {
        String name = "test";
        String description = "secondDescription";
        String category = "6";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.setParameter("category",category);

        request.addParameter("id","9");
        test = new EditActivityCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeInvalidId() {
        String name = "testDelete";
        String description = "test";
        String category = "1";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.setParameter("category",category);

        request.addParameter("id","");
        test = new EditActivityCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeTextId() {

        request.addParameter("id","tet");
        test = new EditActivityCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeNoneExistId() {
        String name = "testCategory";
        String description = "test";
        String category = "1";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.setParameter("category",category);

        request.addParameter("id","9");
        test = new EditActivityCommand();
        assertTrue(test.execute(request,response).contains(Path.COMMAND__ADMIN_EDIT_ACTIVITY));
    }

}