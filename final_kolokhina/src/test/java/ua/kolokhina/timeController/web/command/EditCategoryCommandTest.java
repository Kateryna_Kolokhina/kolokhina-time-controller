package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class EditCategoryCommandTest {

    private static EditCategoryCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new EditCategoryCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
//            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateDataTrue() {
        String name = "test5";
        String description = "secondDescription";
        test= new EditCategoryCommand();
        assertTrue(test.validateData(name,description));
    }

    @Test
    void executeNoneExistId() {

        request.addParameter("id","500");
        test = new EditCategoryCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeDefaultId() {

        request.addParameter("id","1");
        test = new EditCategoryCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void executeInvalidData() {
        String name = "test";
        String description = "";
        request.setParameter("name",name);
        request.setParameter("description",description);
        test = new EditCategoryCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeTrue() {
        String name = "test5";
        String description = "secondDescription";
        request.setParameter("name",name);
        request.setParameter("description",description);
        request.addParameter("id","2");
        test = new EditCategoryCommand();

        assertTrue(test.execute(request,response).contains(Path.COMMAND__ADMIN_EDIT_CATEGORY));
    }
}