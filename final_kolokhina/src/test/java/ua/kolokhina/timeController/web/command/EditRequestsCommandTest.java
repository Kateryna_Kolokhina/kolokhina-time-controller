package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class EditRequestsCommandTest {

    private static EditRequestsCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        BasicConfigurator.configure();
        test = new EditRequestsCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {
            ScriptRunner sr = new ScriptRunner(con);
            Reader reader = new BufferedReader(new FileReader(path));
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void executeWithoutParameters() {
        test = new EditRequestsCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeAdd() {
        request.setParameter("add","add");
        request.setParameter("1","confirm");
        request.setParameter("3","cancel");

        test = new EditRequestsCommand();
        assertEquals(Path.COMMAND__ADMIN_LIST_REQUESTS,test.execute(request,response));
    }

    @Test
    void executeAddWithoutParameters() {
        request.setParameter("add","add");

        test = new EditRequestsCommand();
        assertEquals(Path.COMMAND__ADMIN_LIST_REQUESTS,test.execute(request,response));
    }
    @Test
    void executeDelete() {
        request.setParameter("delete","delete");
        request.setParameter("1","confirm");
        request.setParameter("3","cancel");

        test = new EditRequestsCommand();
        assertEquals(Path.COMMAND__ADMIN_LIST_REQUESTS,test.execute(request,response));
    }

    @Test
    void executeDeleteWithoutParameters() {
        request.setParameter("delete","delete");

        test = new EditRequestsCommand();
        assertEquals(Path.COMMAND__ADMIN_LIST_REQUESTS,test.execute(request,response));
    }

    @Test
    void executeAddDelete() {
        request.setParameter("add","add");
        request.setParameter("delete","delete");
        test = new EditRequestsCommand();
        assertEquals(Path.COMMAND__ADMIN_LIST_REQUESTS,test.execute(request,response));
    }
}