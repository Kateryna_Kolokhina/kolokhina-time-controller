package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.Role;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class LoginCommandTest {


    private static LoginCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        BasicConfigurator.configure();
        test = new LoginCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            Reader reader = new BufferedReader(new FileReader(path));
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateDataTrue() {
        String login = "test";
        String password = "passw";
        LoginCommand command = new LoginCommand();
        assertTrue(command.validateData(login,password));
    }
    @Test
    void validateDataFalse() {
        String login = "test";
        String password = null;
        LoginCommand command = new LoginCommand();
        assertFalse(command.validateData(login,password));
    }
    @Test
    void validateDataEmpty() {
        String login = "";
        String password = "passw";
        LoginCommand command = new LoginCommand();
        assertFalse(command.validateData(login,password));
    }

    @Test
    void executeAdmin() {

        request.addParameter("login","AdminKate");
        request.addParameter("password","Password0");

        test = new LoginCommand();

        assertEquals(Path.COMMAND__ADMIN_LIST_ACCOUNTS,test.execute(request,response));
    }
    @Test
    void executeClient() {

        request.addParameter("login","UserKate");
        request.addParameter("password","Testpassword0");

        test = new LoginCommand();

        assertEquals(Path.COMMAND__USER_ACTIVITIES,test.execute(request,response));
    }
    @Test
    void executeAlreadySighIn() {

        request.addParameter("login","UserKate");
        request.addParameter("password","Testpassword0");

        MockHttpSession session = new MockHttpSession();
        session.setAttribute("userRole", Role.ADMIN);
        request.setSession(session);
        test = new LoginCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeUserNotExist() {

        request.addParameter("login","testes");
        request.addParameter("password","test");

        test = new LoginCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
}