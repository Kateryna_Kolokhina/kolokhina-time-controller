package ua.kolokhina.timeController.web.command;

import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.Role;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LogoutCommandTest {

    private static LogoutCommand test;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
    }

    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void executeSighIn() {

        MockHttpSession session = new MockHttpSession();
        session.setAttribute("userRole", Role.ADMIN);
        request.setSession(session);
        test = new LogoutCommand();

        assertEquals(Path.PAGE__LOGIN,test.execute(request,response));
    }
    @Test
    void executeNotSighIn() {

        test = new LogoutCommand();
        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
}