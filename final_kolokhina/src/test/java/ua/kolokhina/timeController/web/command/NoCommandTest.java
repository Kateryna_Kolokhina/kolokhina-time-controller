package ua.kolokhina.timeController.web.command;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;

import static org.junit.jupiter.api.Assertions.*;

class NoCommandTest {

    @Test
    void execute() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        NoCommand test = new NoCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
}