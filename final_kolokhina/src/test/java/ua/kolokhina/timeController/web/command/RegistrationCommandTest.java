package ua.kolokhina.timeController.web.command;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.Role;
import ua.kolokhina.timeController.db.DBManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class RegistrationCommandTest {

    private static RegistrationCommand test;
    private static DBManager dbManager;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();


        BasicConfigurator.configure();
        test = new RegistrationCommand();

        dbManager = DBManager.getInstance(true);

        String path = "pro/sql/Test.sql";
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password")) {

            ScriptRunner sr = new ScriptRunner(con);
            //Creating a reader object
            Reader reader = new BufferedReader(new FileReader(path));
            //Running the script
            sr.runScript(reader);
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @AfterEach
    void tearDown() {
        try (Connection con = DriverManager.getConnection ("jdbc:mysql://localhost:3306/?user=root&password=password");
             Statement statement = con.createStatement()) {
            String sql ="DROP SCHEMA IF EXISTS `test`;";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    void validateDataFalse() {
        String login = "test";
        String password = null;
        String email = "email";
        String conf_passw = "password";
        test = new RegistrationCommand();
        assertFalse(test.validateData(login,password,email,conf_passw));
    }
    @Test
    void validateInvalidData() {
        String login = "test";
        String password = "password";
        String email = "email";
        String conf_passw = "password";
        test = new RegistrationCommand();
        assertFalse(test.validateData(login,password,email,conf_passw));
    }

    @Test
    void executeAlreadySighIn() {

        request.addParameter("login","UserKate");
        request.addParameter("password","Testpassword0");
        request.addParameter("email","kolokhin@gmai.com");
        request.addParameter("password_confirmation","Testpassword0");
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("userRole", Role.ADMIN);
        request.setSession(session);
        test = new RegistrationCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }

    @Test
    void executeExist() {

        request.addParameter("login","UserKate");
        request.addParameter("password","Testpa0");
        request.addParameter("email","kolokhin@gmai.com");
        request.addParameter("password_confirmation","Testpa0");

        test = new RegistrationCommand();

        assertEquals(Path.PAGE__ERROR_PAGE,test.execute(request,response));
    }
    @Test
    void execute() {

        request.addParameter("login","UserKate67");
        request.addParameter("password","Testpa90");
        request.addParameter("email","kolokhins@gmail.com");
        request.addParameter("password_confirmation","Testpa90");

        test = new RegistrationCommand();

        assertEquals(Path.COMMAND__USER_ACTIVITIES,test.execute(request,response));
    }
}