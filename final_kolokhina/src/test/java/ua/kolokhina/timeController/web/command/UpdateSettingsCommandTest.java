package ua.kolokhina.timeController.web.command;

import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import ua.kolokhina.timeController.Path;
import ua.kolokhina.timeController.db.UserDao;
import ua.kolokhina.timeController.db.entity.User;

import static org.junit.jupiter.api.Assertions.*;

class UpdateSettingsCommandTest {

    private static UpdateSettingsCommand test;
    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        BasicConfigurator.configure();
    }


    @AfterEach
    void tearDown() {
    }

    @Test
    void execute() {

        User user = new UserDao(true).findUserByLogin("UserKate");

        MockHttpSession session = new MockHttpSession();
        session.setAttribute("user", user);
        session.setAttribute("localeToSet", "ua");
        request.setSession(session);
        test = new UpdateSettingsCommand();

        assertEquals(Path.COMMAND__VIEW_SETTINGS,test.execute(request,response));
    }

}