package ua.kolokhina.timeController.web.command;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import ua.kolokhina.timeController.Path;

import static org.junit.jupiter.api.Assertions.*;

class ViewSettingsCommandTest {

    @Test
    void execute() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();

        ViewSettingsCommand test = new ViewSettingsCommand();

        assertEquals(Path.PAGE__SETTINGS,test.execute(request,response));
    }
}